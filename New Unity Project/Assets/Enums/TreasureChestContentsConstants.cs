﻿using UnityEngine;
using System.Collections;

public static class TreasureChestContentsConstants {

    public enum TreasureTypes
    {
        TREASURE_KEY = 0,
        ABILITY = 1,
        TREASURE_SIDEQUESTITEM = 2,
        TREASURE_QUESTITEM = 3
    };
}
