﻿//This is the generic class for the different attacks (ie tap, flick, etc.)

using UnityEngine;
using System.Collections;

public class AttackTypeControl : MonoBehaviour {

    protected BoxCollider2D boxCollider;
    protected BattlingCharacterControl playerRef;

    public int damage;
    public int MPCost;

	// Use this for initialization
	protected virtual void Start () {
        boxCollider = GetComponent<BoxCollider2D>();
        playerRef = transform.parent.GetComponent<BattlingCharacterControl>();
	}
	
	// Update is called once per frame
	protected virtual void Update () {
	
	}

    //Tells player that gesture required to attack was accomplished
    public void successfulAttack()
    {
        playerRef.successfulGestureOccurred();
    }

    public override string ToString()
    {
        return "Attack!";
    }

}
