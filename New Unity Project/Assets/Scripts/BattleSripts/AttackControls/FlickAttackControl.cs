﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class FlickAttackControl : AttackTypeControl {

	// Use this for initialization
	protected override void Start () {
        base.Start();
        GetComponent<FlickGesture>().Flicked += flickHandler;
    }
	
    private void flickHandler(object o, System.EventArgs e)
    {
        successfulAttack();
    }

    public override string ToString()
    {
        return "Flick!";
    }
}
