﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class TapAttackControl : AttackTypeControl {

	// Use this for initialization
	protected override void Start()
    {
        base.Start();
        GetComponent<TapGesture>().Tapped += tapHandler;
    }
	
	// Update is called once per frame
	protected override void Update () {
	
	}

    private void tapHandler(object o, System.EventArgs e)
    {
        successfulAttack();  //yay you tapped the square!
    }

    public override string ToString()
    {
        return "Tap!";
    }

}
