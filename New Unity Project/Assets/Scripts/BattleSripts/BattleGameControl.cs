﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BattleGameControl : MonoBehaviour {

    public const int EXP_NEEDED_TO_LEVEL_UP = 100;
    public const float SCREEN_SHAKE_TIME = 0.07f;
    private int LevelUpAttackBonus, LevelUpDefenseBonus;
    private int LevelUpUpgradeChoice;  //[0-2]
    LevelUpUpgradeSelectorControl UpgradeSelector;

    public BattleEnemyControl[] enemyPrefabs;
    [HideInInspector] public Transform enemyContainer;

    public Vector3 defaultPlayerPosition = new Vector3(-5,-2,0);
    public Vector3[] defaultEnemyPositions = { new Vector3(5,-2,0), new Vector3(6,-1,0), new Vector3(6,-3,0) };

    public enum BattleState { WaitingForInput, ChoosingTarget, Attacking, BattleOver };
    public BattlingCharacterControl[] characters;   //Character 0 is dog player
    [HideInInspector] public BattleHumanControl humanCharacter;      //This is the human player who does not get a turn

    [HideInInspector] public BattleState currState;
    private int currPlayerTurn;
    private int numPlayers;

    [HideInInspector] public TextMesh battleText;
    public bool ignoreTaps;

    private Text healthText;
    private Text MPText;
    private Text rechargeText;
    private Text EXPText;

    private GameController root;
    private EnemyScript overworldEnemyRef;

    private int expForBattle;
    public int TotalPlayerEXP;

    private bool endOfBattleHandled;
    private bool levelUpUpgradeSelected;

    private ItemsMenuOptionControl battleInventory;

	// Use this for initialization
	void Start () {

        root = transform.parent.GetComponent<GameController>();
        humanCharacter = transform.FindChild("BattleHuman").GetComponent<BattleHumanControl>();
        battleInventory = transform.FindChild("BattleItems").GetComponent<ItemsMenuOptionControl>();

        UpgradeSelector = GameObject.Find("LevelUpUpgradeSelector").GetComponent<LevelUpUpgradeSelectorControl>();

        currState = BattleState.WaitingForInput;
        currPlayerTurn = 0;
        numPlayers = 2; //For now
        battleText = transform.FindChild("BattleText").GetComponent<TextMesh>();
        //battleText.text = characters[currPlayerTurn].gameObject.tag + ", what do you want to do?";
        ignoreTaps = false;

        healthText = GameObject.Find("HealthText").GetComponent<Text>();
        rechargeText = GameObject.Find("CooldownText").GetComponent<Text>();
        rechargeText.text = "";
        MPText = GameObject.Find("MPText").GetComponent<Text>();
        EXPText = GameObject.Find("ExperienceText").GetComponent<Text>();
        EXPText.text = "EXP: " + TotalPlayerEXP;

        LevelUpAttackBonus = 0;
        LevelUpDefenseBonus = 1;  //these alternate between 0 and 1 each level

        enemyContainer = new GameObject("BattleEnemyContainer").transform;
        enemyContainer.transform.SetParent(gameObject.transform);
        enemyContainer.transform.localPosition = Vector3.zero;
    }

    void onAwake()
    {
        //Initialize players
        characters[0].transform.position = defaultPlayerPosition;
        int i = 0;
        foreach (BattlingCharacterControl enemy in characters)
        {
            if (enemy == characters[0])
                break;
            enemy.transform.position = defaultEnemyPositions[i++];
        }

    }
	
	// Update is called once per frame
	void Update () {
        healthText.text = "Health: " + characters[0].currHealth + "/" + characters[0].maxHealth;
        MPText.text = "DP: " + characters[0].currMP + "/" + characters[0].maxMP;

        if (root.inBattle)
            rechargeText.text = "Recharge: " + (int) (((float) humanCharacter.currentCooldownTime / (float) humanCharacter.cooldownTime) * 100.0f) + "%";
        else
            rechargeText.text = "";
        
        //Only in WaitingForInput phase can you use items from the inventory
        if (currState == BattleState.WaitingForInput)
        {
            battleInventory.canBeOpened = true;
        } else
        {
            battleInventory.canBeOpened = false;
        }

        //Check win conditions
        if (currState == BattleState.BattleOver)
        {
            if (characters[0].currHealth > 0)
            {
                //battleText.text = "Player Wins!";
            }
            else
            {
                //battleText.text = "Player Lost!";
            }
            if (!endOfBattleHandled)
            {
                endOfBattleHandled = true;
                StartCoroutine(BattleOverWaitTime());
            }
        }
    }

    public void enemyTapped(BattleEnemyControl enem)
    {
        //If "choosing target" and is player's turn, set target
        if (currState == BattleState.ChoosingTarget && characters[0].isYourTurn)
        {
            characters[0].setTarget(enem);
            enem.boxCollider.enabled = false;
            currState = BattleState.Attacking;
        }
    }

    public void characterDied(BattlingCharacterControl character)
    {
        numPlayers--;
        if (character != characters[0])
        {
            //Compute EXP
            int exp = ComputeEXP(character.level, characters[0].level);
            Debug.Log("Gained " + exp + " exp");
            expForBattle += exp;
            character.gameObject.SetActive(false);
            //Destroy(character.gameObject);
        }
        if (numPlayers == 1)
        {
            currState = BattleState.BattleOver;
        }
    }

    public void startAttackPhase()
    {
        StartCoroutine(WaitForPlayerTurnToEnd());
    }

    IEnumerator WaitForPlayerTurnToEnd()
    {
        //Wait for attack process to end
        while (characters[currPlayerTurn].busyAttacking)
        {
            yield return null;
        }
        EndTurn();
    }

    private void EndTurn()
    {
        //Swap character turns
        characters[currPlayerTurn].isYourTurn = false;
        currPlayerTurn = (currPlayerTurn + 1) % numPlayers;
        Debug.Log("CurrPlayer: " + currPlayerTurn);
        characters[currPlayerTurn].isYourTurn = true;
        if (numPlayers > 1)
        {
            currState = BattleState.WaitingForInput;
        }
        else
        {
            currState = BattleState.BattleOver;
        }
    }

    IEnumerator BattleOverWaitTime()
    {
        yield return new WaitForSeconds(1);

        //Show increase in EXP
        /*TotalPlayerEXP += expForBattle;

        //Check for level up (while loop just in case they gain more than one level, though doubtful)
        while (TotalPlayerEXP >= EXP_NEEDED_TO_LEVEL_UP)
        {
            characters[0].level++;
            TotalPlayerEXP -= EXP_NEEDED_TO_LEVEL_UP;
        }
        EXPText.text = "EXP: " + TotalPlayerEXP;*/

        Debug.Log("Adding " + expForBattle + " exp in coroutine");
        StartCoroutine(IncrementEXPTime(expForBattle));

        Debug.Log("Battle Over Time Done");
        root.inBattle = false;
        //Remove overworld enemy
        if (overworldEnemyRef)
        {
            overworldEnemyRef.OnDefeat();
            Destroy(overworldEnemyRef.gameObject);
        }
        currState = BattleState.WaitingForInput;
        expForBattle = 0;
        if (enemyContainer)
        {
            Destroy(enemyContainer.gameObject);
        }
    }

    IEnumerator IncrementEXPTime(int expAmount)
    {
        int i = expAmount;
        //Show increase in EXP
        //TotalPlayerEXP += expForBattle;

        //Increment exp one at a time to look like it's adding up
        while (i > 0)
        {
            TotalPlayerEXP += 1;
            i--;
            if (TotalPlayerEXP >= EXP_NEEDED_TO_LEVEL_UP)
            {
                characters[0].level++;
                TotalPlayerEXP -= EXP_NEEDED_TO_LEVEL_UP;
                StartCoroutine(OnLevelUp());
            }
            EXPText.text = "EXP: " + TotalPlayerEXP;
            yield return new WaitForSeconds(0.1f);
        }
    }

    public void initializeBattle(EnemyScript enemyData)
    {
        if (!enemyContainer)
            enemyContainer = new GameObject("BattleEnemyContainer").transform;
        enemyContainer.transform.SetParent(gameObject.transform);
        enemyContainer.transform.localPosition = Vector3.zero;

        battleText.text = "";
        overworldEnemyRef = enemyData;
        endOfBattleHandled = false;

        //Instantiate and initialize enemy
        int i = 0; //for when we have battles with more than one enemy
        BattleEnemyControl newEnemy = Instantiate(enemyPrefabs[enemyData.enemyID], Vector3.zero, Quaternion.identity) as BattleEnemyControl;

        newEnemy.transform.SetParent(enemyContainer);
        newEnemy.transform.localPosition = defaultEnemyPositions[i++];
        newEnemy.maxHealth = enemyData.maxHealth;
        newEnemy.currHealth = newEnemy.maxHealth;
        newEnemy.attack = enemyData.attack;
        newEnemy.defense = enemyData.defense;
        newEnemy.speed = enemyData.speed;
        newEnemy.attackSpeed = enemyData.attackSpeed;
        newEnemy.playerRef = characters[0] as BattlePlayerControl;

        characters[1] = newEnemy;
        numPlayers = 2;
        currState = BattleState.WaitingForInput;
        root.inBattle = true;
    }

    public void playerLandedHit(int damage)
    {
        humanCharacter.incrementCooldownTimer(damage);
    }

    public void OnItemUsed(int characterID, int HPBoost, int MPBoost)
    {
        characters[characterID].healCharacter(HPBoost);
        characters[characterID].restoreMP(MPBoost);
        //Close item inventory
        battleInventory.CloseItemMenu();
        //End Player turn
        if (root.inBattle)
        {
            //If in battle, end the turn
            EndTurn();
        }
    }

    private int ComputeEXP(float enemy, float player)
    {
        return (int) Mathf.Round((enemy / player)*Mathf.Pow((enemy+1) / (enemy + player + 1), 2) + 1);
    }

    IEnumerator OnLevelUp()
    {
        characters[0].attack += LevelUpAttackBonus;
        characters[0].defense += LevelUpDefenseBonus;
        int temp = LevelUpAttackBonus;
        LevelUpAttackBonus = LevelUpDefenseBonus;
        LevelUpDefenseBonus = temp;

        UpgradeSelector.OpenUpgradeSelection();
        root.EnablePlayerMovement(false);
        root.GamePaused = true;

        //Here is where we would present the choice
        while (!levelUpUpgradeSelected)
        {
            //TODO: Selection screen for player
            yield return null;
        }

        UpgradeSelector.CloseUpgradeSelection();
        root.EnablePlayerMovement(true);
        root.GamePaused = false;

        switch(LevelUpUpgradeChoice)
        {
            case 0:
                characters[0].maxHealth += 5;
                break;
            case 1:
                characters[0].maxMP += 5;
                break;
            case 2:
                characters[0].attackSpeed += 1;
                break;
        }
        //Make sure to reset this flag for the next level
        levelUpUpgradeSelected = false;

        characters[0].currHealth = characters[0].maxHealth;
        characters[0].currMP = characters[0].maxMP;

    }

    public bool isPaused()
    {
        if (battleInventory.consumablesMenuOpen)
        {
            return true;
        }
        return false;
    }

    public void ShakeCamera(float damage)
    {
        root.AddScreenShake(SCREEN_SHAKE_TIME, 0.5f * Mathf.Sqrt(damage));
    }

    public void SelectUpgrade(int selection)
    {
        Debug.Log("Selected " + selection);
        LevelUpUpgradeChoice = selection;
        levelUpUpgradeSelected = true;
    }

}
