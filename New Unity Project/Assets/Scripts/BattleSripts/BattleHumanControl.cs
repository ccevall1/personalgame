﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class BattleHumanControl : MonoBehaviour
{
    private const int smallHealModifier = 1;
    private const int smallMPModifier = 2;
    private const int attackBoostModifier = 1;
    private const int defenseBoostModifier = 2;
    private const int largeHealModifier = 5;

    public BattlePlayerControl dogRef;
    public int cooldownTime;
    [HideInInspector] public int currentCooldownTime;
    private int level = 1;
    private int currEXP = 1;

    public GameObject GlowEffect;  //particle effect for when human is charged

    private enum Abilities { Heal, BoostAttack, BoostDefense, RestoreMP, MoreHeal };
    private int numAbilitiesUnlocked = 0;

    public GameObject EffectText; //prefab for text indicating what human did
    private string effectString;

    public GameObject LevelUpText; //prefab to display when human levels up

    LineRenderer EXPBar;

    // Use this for initialization
    void Start()
    {
        GetComponent<TapGesture>().Tapped += tapHandler;
        level = ComputeLevelFromEXP(currEXP);
        cooldownTime = ComputeCooldownTimeFromLevel(level);
        EXPBar = transform.FindChild("EXPBar").GetComponent<LineRenderer>();
        RewardEXP(0);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void tapHandler(object o, System.EventArgs e)
    {
        if (currentCooldownTime < cooldownTime)
        {
            Debug.Log("Not ready");
        } else if (currentCooldownTime == cooldownTime)
        {
            Debug.Log("Doing the thing");
            Abilities whatToDo = (Abilities) Random.Range(0, numAbilitiesUnlocked + 1); //+1 because upper bound exclusive
            switch (whatToDo)
            {
                case Abilities.Heal:
                    dogRef.healCharacter(smallHealModifier * level);
                    effectString = "+" + smallHealModifier*level + " HP";
                    break;
                case Abilities.BoostAttack:
                    dogRef.attackBoost = attackBoostModifier * level;
                    effectString = "+" + attackBoostModifier*level + " Attack";
                    break;
                case Abilities.BoostDefense:
                    dogRef.defenseBoost = defenseBoostModifier * level;
                    effectString = "+" + defenseBoostModifier*level + " Defense";
                    break;
                case Abilities.RestoreMP:
                    dogRef.restoreMP(smallMPModifier * level);
                    effectString = "+" + smallMPModifier*level + " MP";
                    break;
                case Abilities.MoreHeal:
                    dogRef.healCharacter(largeHealModifier * level);
                    effectString = "+" + largeHealModifier*level + " HP";
                    break;
            }
            currentCooldownTime = 0;
            GlowEffect.SetActive(false);
            TextMesh textMesh = (Instantiate(EffectText, transform.position + Vector3.up, Quaternion.identity) as GameObject).GetComponent<TextMesh>();
            textMesh.text = effectString;
            RewardEXP(1);  //For now every time used, increment exp by 1
        }
    }

    public void incrementCooldownTimer(int amount)
    {
        currentCooldownTime = Mathf.Clamp(currentCooldownTime + amount, 0, cooldownTime);
        if (currentCooldownTime == cooldownTime)
        {
            GlowEffect.SetActive(true);
        }
    }

    public void LevelUp()
    {
        level++;
        Debug.Log("Current Human level: " + level);
        cooldownTime = ComputeCooldownTimeFromLevel(level);
        // Set current cooldown to fully charged as bonus for leveling up
        currentCooldownTime = cooldownTime;
        EXPBar.SetPosition(1, Vector3.zero);
        GlowEffect.SetActive(true);
        Instantiate(LevelUpText, transform.position + 2 * Vector3.up, Quaternion.identity);
    }

    public void RewardEXP(int exp)
    {
        currEXP += exp;
        int newLevel = ComputeLevelFromEXP(currEXP);

        //Update exp bar
        float percentExp = (float)(currEXP - ComputeEXPNeededForNextLevel(level - 1)) / (float)ComputeEXPNeededForNextLevel(level);
        Debug.Log("Percent Human EXP: " + percentExp);
        EXPBar.SetPosition(1, new Vector3(0, percentExp, 0));

        //Check if new level
        if (newLevel > level)
        {
            LevelUp();
        }
    }

    ///////////////////// Math-y stuff ////////////////////////////

    public int ComputeLevelFromEXP(int exp)
    {
        return (int) Mathf.Floor(Mathf.Sqrt(exp));
    }

    public int ComputeCooldownTimeFromLevel(int level)
    {
        return (int)Mathf.Round(2 * Mathf.Sqrt(level));
    }

    public int ComputeEXPNeededForNextLevel(int currLevel)
    {
        return (int)Mathf.Round(Mathf.Pow(currLevel+1, 2));
    }
}
