﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class BattleTouchControl : MonoBehaviour {

    public BattleGameControl root;
    private Camera cam;
    private BoxCollider2D boxCollider;
    private float worldUnitsPerScreenPixel, cameraWidth, cameraHeight;

	// Use this for initialization
	void Start () {
        GetComponent<TapGesture>().Tapped += tapHandler;

        cam = transform.parent.GetComponent<Camera>();
        boxCollider = GetComponent<BoxCollider2D>();
        worldUnitsPerScreenPixel = cam.orthographicSize / cam.pixelHeight;
        cameraHeight = worldUnitsPerScreenPixel * cam.pixelHeight;
        cameraWidth = worldUnitsPerScreenPixel * cam.pixelWidth;
        boxCollider.size = new Vector2(cameraWidth * 2, cameraHeight * 2);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    private void tapHandler(object o, System.EventArgs e)
    {
        
    }
}
