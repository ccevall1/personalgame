﻿using System.Collections;
using TouchScript.Gestures;
using UnityEngine;

public abstract class BattlingCharacterControl : MonoBehaviour {

    //Stats
    public int level;
    public int maxHealth;
    public int currHealth;
    public int maxMP, currMP;
    public int attack;
    public int defense;
    public int speed; //Literal movement speed for animations
    public float attackSpeed; //how often you can attack on a turn
    private float inverseAttackSpeed;

    //Modifiers
    public int attackBoost, defenseBoost;

    public AttackTypeControl[] attacks;
    [HideInInspector] public AttackTypeControl currAttack;

    public bool isYourTurn;
    public bool isSelected;
    public bool busyAttacking;

    public GameObject damageText;

    protected bool isInPosition;
    protected BattleGameControl root;
    private SpriteRenderer img;
    private BattlingCharacterControl attackingMe;
    [HideInInspector] public BattlingCharacterControl target;

    private Vector3 defaultPosition;
    private Vector3 targetPosition;

    public float attackBufferTimer;
    private float maxAttackBufferTime;

    private Animator animator;

    [HideInInspector]
    public BoxCollider2D boxCollider;

    // Use this for initialization
    protected virtual void Start () {
        currHealth = maxHealth;
        currMP = maxMP;

        isInPosition = false;
        isSelected = false;
        busyAttacking = false;

        GetComponent<TapGesture>().Tapped += tapHandler;
        root = GameObject.Find("BattleController").GetComponent<BattleGameControl>();
        img = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        boxCollider = GetComponent<BoxCollider2D>();

        defaultPosition = transform.localPosition;

        inverseAttackSpeed = 1f / attackSpeed;

	}
	
	// Update is called once per frame
	protected virtual void Update () {

        //Lerp movements towards and away from target
        if (busyAttacking) //attacking phase
        {
            if (Vector2.Distance(transform.localPosition, targetPosition) > 1)
            {
                transform.Translate((targetPosition - transform.localPosition) * Time.deltaTime * speed);
            }
        } else  //end attack phase
        {
            if (Vector2.Distance(transform.localPosition, defaultPosition) > 0)
            {
                transform.Translate((defaultPosition - transform.localPosition) * Time.deltaTime * speed);
            }
        }

        //Timers
        if (attackBufferTimer > 0)
        {
            attackBufferTimer -= Time.deltaTime;
        }

	}

    public virtual void attackOpponent()
    {
        targetPosition = target.transform.localPosition;
        target.attackingMe = this;
        target.maxAttackBufferTime = inverseAttackSpeed;
        StartCoroutine(attackAnimation());
    }

    public virtual void takeDamage(int att)
    {
        if (attackBufferTimer <= 0)
        {
            attackBufferTimer = maxAttackBufferTime;
            isSelected = false;

            //Get actual damage done
            int damageDone = Mathf.Clamp((att) - (defense + defenseBoost), 0, att);

            //Create damage text
            GameObject o = Instantiate(damageText, (Vector3.zero), Quaternion.identity) as GameObject;
            o.transform.SetParent(transform);
            o.transform.localPosition = Vector3.up;
            o.GetComponent<TextMesh>().text = "" + damageDone;

            //Actually decrement health
            currHealth -= damageDone;
            if (currHealth <= 0)
            {
                root.characterDied(this);
            }

            //Shake screen for some extra juice
            root.ShakeCamera(0.5f * Mathf.Sqrt(damageDone));

        }
    }

    protected virtual void tapHandler(object o, System.EventArgs e)
    {
        
    }

    IEnumerator attackAnimation()
    {
        root.ignoreTaps = true;
        busyAttacking = true;
        boxCollider.enabled = false;
        yield return new WaitForSeconds(2);
        root.ignoreTaps = false;
        busyAttacking = false;
        boxCollider.enabled = true;
        resetTurnConditions();
    }

    protected virtual void resetTurnConditions()
    {
        //empty virtual method enemy overrides
        foreach (AttackTypeControl a in attacks)
        {
            a.gameObject.SetActive(false);
        }
    }

    public void healCharacter(int amount)
    {
        currHealth = Mathf.Clamp(currHealth + amount, 0, maxHealth);
    }

    public void restoreMP(int amount)
    {
        currMP = Mathf.Clamp(currMP + amount, 0, maxMP);
    }

    public virtual void selectAttackType(int choice)
    {
        currAttack = attacks[choice];
        currAttack.gameObject.SetActive(true);
        currMP -= currAttack.MPCost;
    }

    public void setTarget(BattlingCharacterControl targ)
    {
        target = targ;
        attackOpponent();
        root.startAttackPhase();
    }

    public virtual void successfulGestureOccurred()
    {
        Debug.Log("Successful attack!");
        //this is the part where you attack
        //if distance close enough, target takes damage
        if (Vector3.Distance(transform.localPosition, target.transform.localPosition) < 1.1f)
        {
            target.takeDamage((attack + attackBoost) * currAttack.damage);
        }
    }

}
