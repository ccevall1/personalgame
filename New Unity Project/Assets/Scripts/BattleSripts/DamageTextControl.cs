﻿using UnityEngine;
using System.Collections;

public class DamageTextControl : MonoBehaviour {

    public float lifetime;
    private float timer;
    private float lifeTimeInverse; //inverse because multiplying is faster than dividing
    private TextMesh damageText;

	// Use this for initialization
	void Start () {
        damageText = GetComponent<TextMesh>();
        timer = lifetime;
        lifeTimeInverse = 1f / lifetime;
        GetComponent<Renderer>().sortingLayerName = "UI";
    }
	
	// Update is called once per frame
	void Update () {
        if (timer > 0)
        {
             timer -= Time.deltaTime;
        } else
        {
            Destroy(gameObject);
        }
        transform.Translate(Vector3.up * Time.deltaTime);
        Color col = damageText.color;
        col.a = timer * lifeTimeInverse;
        damageText.color = col;
	}
}
