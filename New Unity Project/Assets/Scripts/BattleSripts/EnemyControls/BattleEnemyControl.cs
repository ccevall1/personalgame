﻿using UnityEngine;
using System.Collections;
using System;
using TouchScript.Hit;

/**
 * An enemy holds a reference to the player character and on its turn, approaches player, waits, then attacks
**/
public class BattleEnemyControl : BattlingCharacterControl {

    public BattlePlayerControl playerRef;
    public GameObject blockText;
    public SpriteRenderer spr;

    private float delayTimer;
    public float delayTime;

    private float attackTimer;
    public float attackTime;

    public float blockTextAppearTime;
    public GameObject SelectIcon;  //Indicates to player you can select it

    protected override void Start()
    {
        base.Start();
        spr = GetComponent<SpriteRenderer>();
        delayTimer = delayTime;
        SelectIcon = transform.FindChild("SelectionIcon").gameObject;
    }

    protected override void Update()
    {
        base.Update();  //handles lerp to player

        //Basic AI for enemy
        if (isYourTurn)
        {
            SelectIcon.SetActive(false);
            if (delayTimer > 0)
            {
                delayTimer -= Time.deltaTime;
            }
            else
            {
                if (root.currState == BattleGameControl.BattleState.WaitingForInput)
                {
                    selectAttackType(0);
                    //delayTimer = delayTime;
                    root.currState = BattleGameControl.BattleState.ChoosingTarget;
                    Debug.Log("Selected attackType");
                } else if (root.currState == BattleGameControl.BattleState.ChoosingTarget)
                {
                    setTarget(playerRef);
                    //root.startAttackPhase();
                    root.currState = BattleGameControl.BattleState.Attacking;
                    Debug.Log("Chose target");
                    attackTimer = attackTime;
                } else if (root.currState == BattleGameControl.BattleState.Attacking)
                {
                    if (attackTimer > 0)
                    {
                        attackTimer -= Time.deltaTime;
                        if (attackTimer < blockTextAppearTime)
                        {
                            GameObject o = Instantiate(blockText, (Vector3.zero), Quaternion.identity) as GameObject;
                            o.transform.SetParent(playerRef.transform);
                            o.transform.localPosition = 2 * Vector3.up;
                        }
                    } else
                    {
                        currAttack.successfulAttack();
                        attackTimer = 3 * attackTime; //Don't attack again
                    }
                }
            }
        }  //end if isYourTurn
        else
        {
            if (root.currState == BattleGameControl.BattleState.ChoosingTarget)
            {
                SelectIcon.SetActive(true);
            }
            else
            {
                SelectIcon.SetActive(false);
            }
        }
        
    }

    protected override void tapHandler(object o, EventArgs e)
    {
        if (!isYourTurn)
        {
            root.enemyTapped(this);
        }
    }

    protected override void resetTurnConditions()
    {
        base.resetTurnConditions(); //deactivates attack control
        isYourTurn = false;
        delayTimer = 2.0f;
        boxCollider.enabled = true;
    }

    public override void takeDamage(int att)
    {
        int preHitHealth = currHealth;
        base.takeDamage(att);
        //Since not player, inc human cooldown, only if the hit landed
        if (currHealth < preHitHealth)
            root.playerLandedHit(att);
    }

}
