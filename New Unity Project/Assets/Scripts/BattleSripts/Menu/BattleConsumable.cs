﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class BattleConsumable : MonoBehaviour {

    public int ID;
    public int HealthBoost, MPBoost;
    SpriteRenderer rend;
    public Sprite spr;
    public Color selectedColor;

    [HideInInspector]
    public bool isSelected;

    GameController root;
    UIConsumablesControl UIParent;

    BattleConsumablesControl ConsumablesParent;

	// Use this for initialization
    void Start ()
    {
        GetComponent<TapGesture>().Tapped += tapHandler;
        rend = GetComponent<SpriteRenderer>();
        spr = rend.sprite;
        root = transform.root.GetComponent<GameController>();
        ConsumablesParent = transform.parent.parent.GetComponent<BattleConsumablesControl>();
    }

    void Update()
    {
        rend.sprite = spr;
        if (isSelected)
        {
            rend.color = selectedColor;
        }
        else
        {
            rend.color = Color.white;
        }
    }

    private void tapHandler(object o, System.EventArgs e)
    {
        //Broadcast to other consumables that this one was clicked (so others not selected anymore)
        foreach (BattleConsumable con in ConsumablesParent.consumablesSlots)
        {
            if (con != this)
            {
                con.isSelected = false;
            }
        }

        if (isSelected)
        {
            root.OnConsumableUsed(ID, HealthBoost, MPBoost);
            isSelected = false;
        }
        else
        {
            isSelected = true;
        }

    }
}
