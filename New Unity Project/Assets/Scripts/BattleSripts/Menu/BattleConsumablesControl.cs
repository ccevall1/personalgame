﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleConsumablesControl : MonoBehaviour {

    public BattleConsumable[] genericConsumableTypes;
    public BattleConsumable[] consumablesSlots;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void onRefresh(List<int> consumables)
    {
        Debug.Log("Refreshing consumables, " + consumables.Count);
        int i = 0;
        foreach (int id in consumables)
        {
            consumablesSlots[i].ID = id;
            consumablesSlots[i].HealthBoost = genericConsumableTypes[id].HealthBoost;
            consumablesSlots[i].MPBoost = genericConsumableTypes[id].MPBoost;
            consumablesSlots[i].spr = genericConsumableTypes[id].spr;
            consumablesSlots[i].gameObject.SetActive(true);
            i++;
        }
        while (i < consumablesSlots.Length)
        {
            consumablesSlots[i].gameObject.SetActive(false);
            i++;
        }
    }
}
