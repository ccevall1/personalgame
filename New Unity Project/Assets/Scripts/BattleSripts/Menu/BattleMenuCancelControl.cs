﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class BattleMenuCancelControl : MonoBehaviour {

    private ItemsMenuOptionControl parent;
    BoxCollider2D boxCollider;

	// Use this for initialization
	void Start () {
        GetComponent<TapGesture>().Tapped += tapHandler;
        parent = transform.parent.GetComponent<ItemsMenuOptionControl>();
        boxCollider = GetComponent<BoxCollider2D>();
	}

    void Update()
    {
        if (parent.consumablesMenuOpen)
        {
            boxCollider.enabled = true;
        }
        else
        {
            boxCollider.enabled = false;
        }
    }
	
    private void tapHandler(object o, System.EventArgs e)
    {
        parent.CloseItemMenu();
    }
}
