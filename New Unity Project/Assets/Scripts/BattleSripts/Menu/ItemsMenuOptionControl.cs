﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class ItemsMenuOptionControl : MonoBehaviour {

    BattleConsumablesControl ConsumablesMenu;
    public bool consumablesMenuOpen;
    public bool canBeOpened;
    private GameController root;

	// Use this for initialization
	void Start () {
        GetComponent<TapGesture>().Tapped += tapHandler;
        root = transform.root.GetComponent<GameController>();
        consumablesMenuOpen = false;
        ConsumablesMenu = transform.FindChild("BattleConsumables").GetComponent<BattleConsumablesControl>();
        ConsumablesMenu.gameObject.SetActive(true);
        ConsumablesMenu.onRefresh(root.consumablesList.consumables);
        ConsumablesMenu.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
        if (!canBeOpened)
        {
            consumablesMenuOpen = false;
            ConsumablesMenu.gameObject.SetActive(false);
        }
    }

    private void tapHandler(object o, System.EventArgs e)
    {
        if (!canBeOpened)
        {
            consumablesMenuOpen = false;
            ConsumablesMenu.gameObject.SetActive(false);
            return;
        }
        ConsumablesMenu.gameObject.SetActive(true);
        ConsumablesMenu.onRefresh(root.consumablesList.consumables);
        ConsumablesMenu.gameObject.SetActive(false);

        if (consumablesMenuOpen)
        {
            consumablesMenuOpen = false;
            ConsumablesMenu.gameObject.SetActive(false);
        }
        else
        {
            consumablesMenuOpen = true;
            ConsumablesMenu.gameObject.SetActive(true);
        }
    }

    public void CloseItemMenu()
    {
        if (consumablesMenuOpen)
        {
            consumablesMenuOpen = false;
            ConsumablesMenu.gameObject.SetActive(false);
        }
    }
}
