﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class AttackIconControl : MonoBehaviour {

    public int MPCost;
    public bool isSelected;
    
    public int attackIndex; //index in BattlePlayer to distinguish attack types

    private TextMesh MPCostText;

    private BattlePlayerControl playerRef;
    private BattleGameControl root;

	// Use this for initialization
	void Start () {
        GetComponent<TapGesture>().Tapped += tapHandler;
        playerRef = transform.parent.parent.FindChild("BattlePlayer").GetComponent<BattlePlayerControl>();
        root = transform.parent.parent.GetComponent<BattleGameControl>();

        MPCostText = transform.FindChild("MPCost").GetComponent<TextMesh>();
        MPCostText.text = MPCost + "DP";
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void tapHandler(object o, System.EventArgs e)
    {
        if (playerRef.currMP >= MPCost && !root.isPaused())
            playerRef.selectAttackType(attackIndex);
    }

}
