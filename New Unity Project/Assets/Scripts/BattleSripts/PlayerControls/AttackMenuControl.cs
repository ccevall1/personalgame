﻿using UnityEngine;
using System.Collections;

public class AttackMenuControl : MonoBehaviour {

    public GameObject[] attacks;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void InitAttackMenu(int numAbilities)
    {
        for (int i = 0; i < numAbilities; i++)
        {
            attacks[i].SetActive(true);
        }
    }
}
