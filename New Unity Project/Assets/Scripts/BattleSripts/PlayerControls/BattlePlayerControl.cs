﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class BattlePlayerControl : BattlingCharacterControl {

    public float DefenseDelayTime;
    public float DefenseActiveTime;

    private AttackMenuControl attackMenu;
    private GameObject defenseHalo;
    private TextMesh attackText;

    private bool isDefending;
    private float defenseTimer;
    private GameController gameRoot;

    protected override void Start()
    {
        base.Start();
        gameRoot = transform.root.GetComponent<GameController>();
        attackMenu = transform.parent.FindChild("AttackMenu").GetComponent<AttackMenuControl>();
        attackMenu.InitAttackMenu(gameRoot.SaveManager.playerAbilitiesUnlocked);
        defenseHalo = transform.FindChild("DefenseHalo").gameObject;
        GetComponent<TapGesture>().Tapped += defenseTapHandler;
        attackText = GameObject.Find("AttackPromptText").GetComponent<TextMesh>();
        attackText.gameObject.SetActive(false);
    }

    protected override void Update()
    {
        base.Update();  //if busyAttacking, lerps player appropriately and handles attackTimer
        //if WaitingForInput, display options
        if (root.currState == BattleGameControl.BattleState.WaitingForInput && isYourTurn)
        {
            if (!attackMenu.gameObject.activeSelf)
            {
                attackMenu.gameObject.SetActive(true);
                attackMenu.InitAttackMenu(gameRoot.SaveManager.playerAbilitiesUnlocked);
            }
        }
        else
        {
            attackMenu.gameObject.SetActive(false);
        }

        if (defenseTimer > 0)
        {
            defenseTimer -= Time.deltaTime;
        } else
        {
            defenseHalo.SetActive(false);
            isDefending = false;
        }

        if (defenseTimer < DefenseDelayTime - DefenseActiveTime)
        {
            defenseHalo.SetActive(false);
        }

        if (busyAttacking && target.attackBufferTimer <= 0)
        {
            attackText.text = currAttack.ToString();
            attackText.transform.position = target.transform.position + Vector3.up;
            attackText.gameObject.SetActive(true);
        } else
        {
            if (attackText != null)
            {
                attackText.gameObject.SetActive(false);
            }
        }
    }

    public override void takeDamage(int att)
    {
        if (!isDefending)
        {
            base.takeDamage(att);
        } else
        {
            Debug.Log("Blocked attack!");
            defenseBoost += defense + 1; //add additional defense to the player for this turn
            base.takeDamage(att); //subtract off additional defense
            defenseBoost -= defense + 1; //reset back to whatever defense boost they normally have
        }
    }

    protected override void resetTurnConditions()
    {
        base.resetTurnConditions();
    }

    public override void selectAttackType(int choice)
    {
        base.selectAttackType(choice);
        attackMenu.gameObject.SetActive(false);
        root.currState = BattleGameControl.BattleState.ChoosingTarget;
    }

    private void defenseTapHandler(object o, System.EventArgs e)
    {
        if (isDefending) return;

        isDefending = true;
        defenseTimer = DefenseDelayTime;
        defenseHalo.SetActive(true);

    }

}
