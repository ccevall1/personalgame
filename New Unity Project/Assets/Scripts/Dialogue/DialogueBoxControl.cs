﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogueBoxControl : MonoBehaviour {

    Text textBox;

	// Use this for initialization
	void Awake () {
        textBox = transform.FindChild("DialogueText").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetText(string newText)
    {
        textBox.text = newText;
    }

}
