﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class DialogueMap {

    public static Dictionary<string, string> Dialogue = new Dictionary<string, string>()
    {
        {"NPC_SAMPLE_1", "Hello!" },
        {"NPC_SAMPLE_2", "Welcome to the city." },
        {"NPC_SAMPLE_3", "Be sure to watch yourself though, dangerous folks about." },

        {"NPC_CHARLES_1", "How d'you do?" },
        {"NPC_CHARLES_2", "I've been worried about the mayor's health lately..." },

        {"NPC_FORTGUARD_1", "GET OUT! YOU'RE NOT WELCOME HERE!" },

        {"NPC_JENKINS_1", "Dad gum these dang chickens have gone crazy I tell ya!" },
        {"NPC_JENKINS_2", "And it's not just cuz I forget to feed 'em." },
        {"NPC_JENKINS_3", "I leave mahself a note now so I haven't got the slightest idear," },
        {"NPC_JENKINS_4", "of what's causing this hulabaloo." },
        {"NPC_JENKINS_5", "I got it! Mebbe you can sort this out." },
        {"NPC_JENKINS_6", "Yer dog there looks pretty tough, I bet it could scare 'em right." },
        {"NPC_JENKINS_7", "I'll just give ya the key to the barn so you can go calm them birds down." },
        {"NPC_JENKINS_8", "Errrmmmmm...seems I misplaced that darn thing, hehe." },
        {"NPC_JENKINS_9", "Musta left it somewhere in the fields..." },
        {"NPC_JENKINS_10", "Well why don't you go and fetch it fer me? I'm an old man ya know. Tired bones." },
        {"NPC_JENKINS_11", "Did ya get the key yet? I left it in the fields this morning...or maybe yesterday." },
        {"NPC_JENKINS_12", "Great! I knew I left it somewhere behind some corn. Alright, head to the barn and calm those chickens." },
        {"NPC_JENKINS_13", "Thanks so much I really 'ppreciate it ya know." },

        {"NPC_CAVEGUARD_1", "Whoa there, sorry friend, you can't go in there." },
        {"NPC_CAVEGUARD_2", "All the tremors lately have been causing rock slides left and right." },
        {"NPC_CAVEGUARD_3", "Mayor's orders, no one can enter." },
        {"NPC_CAVEGUARD_4", "Now go away." },

        {"NPC_CAVEGUARD_1A", "Oh you talked to the mayor?" },
        {"NPC_CAVEGUARD_1B", "Sounds believable, ok. Here's the key to the gate." },

        {"ABILITY_OBTAINED_2", "You got the power to leap! Flick at certain locations to leap to new heights." },

        {"SIDEQUEST_ITEM_OBTAINED_1", "You found a pretty sea shell. It might be valuable to someone." },

        {"QUEST_ITEM_OBTAINED_1", "You found an amulet. It's probably magical I'm guessing." }
    };

    public static string Get(string key)
    {
        return Dialogue[key];
    }

}
