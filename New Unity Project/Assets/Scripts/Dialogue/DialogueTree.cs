﻿using UnityEngine;
using System.Collections;

public class DialogueTree : MonoBehaviour {

    public enum PostDialogueActions { None, GiveGift, StartBattle, TransitionArea };

    public string[] dialogueStrings;
    private int currDialogueString;
    [HideInInspector] public string firstLine;
    private NPCControl npcRef;

    public PostDialogueActions onDismissAction;

    private GameController root;

    //Battle info
    //TODO

    //Transition info
    [HideInInspector] public ExitDetails transitionInfo;

	// Use this for initialization
	void Start () {
        root = transform.root.GetComponent<GameController>();
        npcRef = GetComponent<NPCControl>();
        currDialogueString = 0;
        firstLine = DialogueMap.Get(dialogueStrings[0]);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public string AdvanceDialogue()
    {
        if (currDialogueString < dialogueStrings.Length - 1)
        {
            // If not last line, return next line
            currDialogueString++;
            return DialogueMap.Get(dialogueStrings[currDialogueString]);
        }
        else
        {
            // Else indicate that there is no next line
            currDialogueString = 0;
            OnDismiss();
            return "\n";
        }
    }

    // Called when dialogue ends
    void OnDismiss()
    {
        Debug.Log("OnDismiss: " + onDismissAction);
        switch(onDismissAction)
        {
            case PostDialogueActions.None:
                {
                    break;
                }
            case PostDialogueActions.GiveGift:
                {
                    if (npcRef != null && npcRef.hasGift)
                    {
                        npcRef.GivePlayerGift();  //checks if they have a gift, give it
                    }
                    break;
                }
            case PostDialogueActions.StartBattle:
                {
                    //TODO: Transition to a battle
                    break;
                }
            case PostDialogueActions.TransitionArea:
                {
                    root.ChangeScene(transitionInfo);
                    break;
                }
        }

        npcRef.EndDialogue();

    }
}
