﻿using UnityEngine;
using System.Collections;

public class NextArrowControl : MonoBehaviour {

    private float timer;
    private float defaultYPosition;

	// Use this for initialization
	void Start () {
        defaultYPosition = transform.localPosition.y;
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        transform.localPosition = new Vector3(0, defaultYPosition + 2 * Mathf.Sin(10*timer), 0);
	}
}
