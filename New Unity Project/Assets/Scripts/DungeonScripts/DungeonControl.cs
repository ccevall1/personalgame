﻿using UnityEngine;
using System.Collections;

public class DungeonControl : MonoBehaviour {

    public SceneMetaData[] Scenes;
    public int currentScene;

    private DungeonManager boardScript;
    private MapReader mr;

    public ChestContentsControl[] genericChestContents;

    private SaveDataManager saveManager;

    private InventoryControl inventory;

    // Use this for initialization
    void Awake () {
        currentScene = 0;
        //Init map before calling InitGame();
        boardScript = GetComponent<DungeonManager>();
        mr = GetComponent<MapReader>();
        saveManager = transform.root.GetComponent<GameController>().SaveManager;
        mr.currFile = Scenes[currentScene].mapFile;
        mr.InitMapReader();
        boardScript.map = mr;
        //LoadDungeon();
        inventory = transform.root.GetComponent<GameController>().Inventory;
    }

    // Update is called once per frame
    void Update () {
	
	}

    public void InitGame()
    {
        Debug.Log("Initializing Dungeon");
        //Instantiate Board and set boardHolder to its transform.
        boardScript.boardHolder = new GameObject("" + Scenes[currentScene].mapID).transform;
        Scenes[currentScene].hasBeenInitialized = true;
        boardScript.boardHolder.gameObject.transform.SetParent(Scenes[currentScene].transform);
        boardScript.SetupScene();
        boardScript.boardHolder.localPosition = Vector3.zero;
    }

    public void LoadDungeon()
    {
        Scenes[currentScene].endScene();    //Deactivates and deletes old data
        mr.currFile = Scenes[currentScene].mapFile;
        mr.InitMapReader();
        InitGame();
        Scenes[currentScene].beginScene();  //Init data and activate scene
        Scenes[currentScene].hasBeenInitialized = true;
    }

    public ChestContentsControl GetTreasureContents(int contentsID)
    {
        return genericChestContents[contentsID];
    }

}
