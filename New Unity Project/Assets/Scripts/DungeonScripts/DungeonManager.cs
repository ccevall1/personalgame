﻿using UnityEngine;
using System.Collections.Generic;

public class DungeonManager : MonoBehaviour
{

    public int columns;                                         //Number of columns in our game board.
    public int rows;                                            //Number of rows in our game board.
    public GameObject[] floorTiles;                             //Array of floor prefabs.
    public GameObject[] wallTiles;                              //Array of wall prefabs
    public GameObject exitTile;
    public GameObject leapPortTile;
    public GameObject dungeonExitTile;
    public GameObject treasureChestTile;
    public GameObject lockedDoorTile;

    //Hold floor
    [HideInInspector]
    public Transform boardHolder;                               //A variable to store a reference to the transform of our Board object.
    private List<Vector3> gridPositions = new List<Vector3>();

    [HideInInspector]
    public MapReader map;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    //Sets up the outer walls and floor (background) of the game board.
    void BoardSetup()
    {
        columns = map.numCols;
        rows = map.numRows;

        int tileType;

        for (int x = 0; x < columns; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                //Read in tile type from Map
                tileType = map.nextCharacter();
                GameObject toInstantiate = floorTiles[0]; //default value

                switch (tileType)
                {
                    case 0:
                        toInstantiate = floorTiles[Random.Range(0, floorTiles.Length)];
                        break; //don't instantiate
                    case 1:
                        toInstantiate = wallTiles[Random.Range(0, wallTiles.Length)];
                        break;
                    case 6:
                        toInstantiate = lockedDoorTile;
                        break;
                }

                //Instantiate the GameObject instance using the prefab chosen for toInstantiate at the Vector3 corresponding to current grid position in loop, cast it to GameObject.
                GameObject instance =
                    Instantiate(toInstantiate, new Vector3(y, x, 1f), Quaternion.identity) as GameObject;

                //Set the parent of our newly instantiated object instance to boardHolder, this is just organizational to avoid cluttering hierarchy.
                instance.transform.SetParent(boardHolder);
            }
        }
    }

    public void SetupScene()
    {
        BoardSetup();
    }
}
