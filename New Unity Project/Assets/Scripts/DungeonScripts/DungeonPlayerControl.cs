﻿using UnityEngine;
using System.Collections;

public class DungeonPlayerControl : MonoBehaviour
{

    public int horizontal, vertical;

    private Vector3 currPosition;
    public float speed, leapSpeed;
    private float inputDelay, delayTime, defaultSpeed;

    public bool canMove;

    private BoxCollider2D coll;
    private GameController root;
    private DungeonControl parent;
    
    //Leap themed things
    private bool isTouchingLeapPort;
    public int currentLeapPort;
    private bool isLeaping;

    // Use this for initialization
    void Start()
    {
        currPosition = transform.localPosition;
        //speed = 0.07f;
        defaultSpeed = speed;
        delayTime = 0.3f;
        inputDelay = 0f;

        coll = GetComponent<BoxCollider2D>();

        root = transform.root.GetComponent<GameController>();
        parent = transform.parent.GetComponent<DungeonControl>();
        canMove = true;
    }

    // Update is called once per frame
    void Update()
    {

        /////////////////// Overworld Movement ////////////////////////////////
        /*if (horizontal != 0)
            vertical = 0;*/
        vertical = 0;

        //Gravity Code
        coll.enabled = false;
        RaycastHit2D hitBelow = Physics2D.Raycast(transform.position, new Vector3(0, -1, 0), 1f);
        coll.enabled = true;
        if (!(hitBelow))   //fall
        {
            if (!isLeaping)
            {
                if (inputDelay > 0)
                {
                    inputDelay -= Time.deltaTime;
                }
                else {
                    currPosition = currPosition + new Vector3(0, -1, 0);
                    setPosition(currPosition);
                }
            } else
            {
                Debug.Log("Can't move");
            }
        } else   //check for horizontal movement
        {
            if (inputDelay > 0)
            {
                inputDelay -= Time.deltaTime;
            }
            else
            {
                if (horizontal != 0)
                {
                    if (canMove)
                    {
                        MovePlayer(horizontal, vertical);
                    }
                }
            }
        }

        if (currPosition != transform.localPosition)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, currPosition, speed*Time.deltaTime);
        }
        else if (Vector3.Distance(transform.localPosition, currPosition) < 0.1f)
        {
            transform.localPosition = currPosition;
            isLeaping = false;
            speed = defaultSpeed;
        }

    }

    private void MovePlayer(int x, int y)
    {
        isTouchingLeapPort = false;
        coll.enabled = false;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, new Vector3(x, y, 0), 1.3f);
        coll.enabled = true;
        if (hit)
        {
            if (hit.transform.gameObject.tag == "Wall")
            {
                return;
            }
            else if (hit.transform.gameObject.tag == "Enemy")
            {
                Debug.Log("Hit enemy");
                //Get enemy object
                EnemyScript enemy = hit.transform.gameObject.GetComponent<EnemyScript>();
                //Tell game controller we're battling and init enemies
                root.initializeBattle(enemy);
                //Disable movement
                root.EnablePlayerMovement(false);
                return;
            }
            else if (hit.transform.gameObject.tag == "Exit")
            {
                ExitControl exit = hit.transform.gameObject.GetComponent<ExitControl>();
                root.ChangeScene(exit);
            } else if (hit.transform.gameObject.tag == "DungeonDoor")
            {
                root.inDungeon = false;
                canMove = false;
                root.player.canMove = true;
                ExitControl exit = hit.transform.gameObject.GetComponent<ExitControl>();
                root.ExitDungeon(exit);
                gameObject.SetActive(false);
            }
            else if (hit.transform.gameObject.tag == "LeapPort")
            {
                //LeapPortControl leap = hit.transform.gameObject.GetComponent<LeapPortControl>();
                isTouchingLeapPort = true;
            }
            else
            {
                Debug.Log("Hit " + hit.transform.gameObject.tag);
            }
        }

        currPosition = currPosition + new Vector3(x, y, 0);
        inputDelay = delayTime;

    }

    public void setMovement(int hor, int ver)
    {
        horizontal = hor;
        vertical = ver;
    }

    //For manual in-game setting of position
    public void setPosition(Vector3 newPosition)
    {
        Debug.Log("Setting Player's Position to " + newPosition);
        currPosition = newPosition;
        transform.localPosition = currPosition;
    }

    public void LeapPortFlickDetected(Vector3 targetPosition, Vector3 portPosition)
    {
        if (transform.position != portPosition)
        {
            return;
        }

        Debug.Log("Leap!");
        currPosition = targetPosition;
        isTouchingLeapPort = false;
        isLeaping = true;
        speed = leapSpeed;
    }

}
