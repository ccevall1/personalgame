﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class LockedDoorControl : MonoBehaviour {

    private const int STATE_UNLOCKED = 1;

    public bool canBeUnlocked;
    //private DungeonControl parent;
    private GameController root;

    private SaveableObject SaveState;
    public string SaveStateID;

    // Use this for initialization
    void Start () {
        GetComponent<TapGesture>().Tapped += tapHandler;
        root = transform.root.GetComponent<GameController>();
        SaveState = GetComponent<SaveableObject>();
    }

    // Update is called once per frame
    void Update () {
        if (SaveState != null)
        {
            int unlocked = SaveState.GetSaveState(SaveStateID);
            if (unlocked == STATE_UNLOCKED)
            {
                gameObject.SetActive(false);
            }
        }
        else
        {
            SaveState = GetComponent<SaveableObject>();
        }
    }

    private void tapHandler(object o, System.EventArgs e)
    {
        //Check if player standing next to door
        float dist;
        if (root.inDungeon)
        {
            dist = Vector3.Distance(transform.position, root.dungeonPlayer.transform.position);
        } else
        {
            dist = Vector3.Distance(transform.position, root.player.transform.position);
        }

        if (dist < 1.1f)
        {
            canBeUnlocked = true;
        } else
        {
            canBeUnlocked = false;
        }

        if (canBeUnlocked)
        {
            bool unlocked = root.TryUnlockDoor();
            if (unlocked)
            {
                SaveState.SetSaveState(SaveStateID, STATE_UNLOCKED);
                gameObject.SetActive(false);
            }
            else
            {
                Debug.Log("Failed to unlock door. No key");
            }
        }

    }
}
