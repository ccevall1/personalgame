﻿using UnityEngine;
using System.Collections;

public class ChestContentsAbility : ChestContentsControl {

    DialogueTree dialogue;

	// Use this for initialization
	void Start () {
        dialogue = GetComponent<DialogueTree>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void AddToInventory()
    {
        base.AddToInventory();
        saveManager.playerAbilitiesUnlocked++;
        //dialogue.dialogueStrings[0] += saveManager.playerAbilitiesUnlocked;
        Debug.Log(dialogue.dialogueStrings[0]);
        root.OpenDialogueTree(dialogue);
    }
}
