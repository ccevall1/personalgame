﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class ChestContentsControl : MonoBehaviour {

    protected SaveDataManager saveManager;
    protected InventoryControl inventory;
    protected GameController root;
    protected GameObject SelectionIcon;

	// Use this for initialization
	void Awake () {
        GetComponent<TapGesture>().Tapped += tapHandler;
        SelectionIcon = transform.FindChild("SelectionIcon").gameObject;
	}

    public void Init()
    {
        root = transform.root.GetComponent<GameController>();
        saveManager = root.SaveManager;
        inventory = root.Inventory;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    private void tapHandler(object o, System.EventArgs e)
    {
        Debug.Log("Tapped contents");
        AddToInventory();
    }

    public virtual void AddToInventory()
    {
        //Empty virtual method
        inventory.isDirty = true;
        Destroy(gameObject);
    }
}
