﻿using UnityEngine;
using System.Collections;

public class ChestContentsKey : ChestContentsControl {

    public override void AddToInventory()
    {
        base.AddToInventory();
        saveManager.dungeonKeys++;
    }
}
