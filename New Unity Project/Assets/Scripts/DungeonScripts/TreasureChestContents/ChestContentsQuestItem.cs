﻿using UnityEngine;
using System.Collections;

public class ChestContentsQuestItem : ChestContentsControl {

    public int level;
    DialogueTree dialogue;
    ExitDetails exitDetails;

    // Use this for initialization
    void Start()
    {
        dialogue = GetComponent<DialogueTree>();
        exitDetails = GetComponent<ExitDetails>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void AddToInventory()
    {
        base.AddToInventory();
        saveManager.questItemsObtained++;
        root.OpenDialogueTree(dialogue);
        dialogue.transitionInfo = exitDetails;
    }

}
