﻿using UnityEngine;
using System.Collections;

public class ChestControlSideQuestItem : ChestContentsControl {

    public int level;
    DialogueTree dialogue;

    // Use this for initialization
    void Start()
    {
        dialogue = GetComponent<DialogueTree>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void AddToInventory()
    {
        base.AddToInventory();
        switch(level)
        {
            case 1:
                saveManager.sideQuestOneObtained = 1;
                break;
            case 2:
                saveManager.sideQuestTwoObtained = 1;
                break;
            case 3:
                saveManager.sideQuestThreeObtained = 1;
                break;
            case 4:
                saveManager.sideQuestFourObtained = 1;
                break;
        }
        root.OpenDialogueTree(dialogue);
    }
}
