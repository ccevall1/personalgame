﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class TreasureChestControl : MonoBehaviour {

    private const int STATE_OPENED = 1;

    public int chestID;
    public Sprite OpenSprite;
    public TreasureChestContentsConstants.TreasureTypes contentsID;

    public bool canBeOpened;
    public bool hasBeenOpened;

    private DungeonControl parent;
    private GameController root;
    private SpriteRenderer rend;

    private SaveableObject SaveState;
    public string SaveStateID;

    // Use this for initialization
    void Start () {
        GetComponent<TapGesture>().Tapped += tapHandler;
        root = transform.root.GetComponent<GameController>();
        parent = GameObject.Find("DungeonController").GetComponent<DungeonControl>();
        rend = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (SaveState == null)
        {
            SaveState = GetComponent<SaveableObject>();
            if (SaveState != null)
            {
                int state = SaveState.GetSaveState(SaveStateID);
                Debug.Log("Current Chest State to " + state);
                if (state == STATE_OPENED)
                {
                    Debug.Log("Setting chest to be opened");
                    canBeOpened = false;
                    hasBeenOpened = true;
                    rend.sprite = OpenSprite;
                }
            }
        }
    }

    private void tapHandler(object o, System.EventArgs e)
    {
        if (hasBeenOpened) return;

        //Check if player standing next to door
        float dist;
        if (root.inDungeon)
        {
            dist = Vector3.Distance(transform.position, root.dungeonPlayer.transform.position);
        }
        else
        {
            dist = Vector3.Distance(transform.position, root.player.transform.position);
        }

        if (dist < 1.1f)
        {
            canBeOpened = true;
        }
        else
        {
            canBeOpened = false;
        }

        if (canBeOpened)
        {
            hasBeenOpened = true;
            rend.sprite = OpenSprite;
            //Do the item reveal here
            ChestContentsControl content = parent.GetTreasureContents((int)contentsID);
            ChestContentsControl clone = (ChestContentsControl) Instantiate(content, transform.position, Quaternion.identity);
            clone.transform.SetParent(transform);
            clone.transform.position = transform.position + Vector3.up;
            clone.Init();

            //Save that it's been opened
            if (SaveState != null)
            {
                Debug.Log("Saving chest opened: " + SaveStateID);
                SaveState.SetSaveState(SaveStateID, STATE_OPENED);
            }
        }
    }
}
