﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConsumablesList : MonoBehaviour {

    public int MAX_INVENTORY_SIZE;

    public List<int> consumables = new List<int>();
    private int numItems;

	// Use this for initialization
	void Start () {
        numItems = consumables.Count;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void AddItem(int consume)
    {
        if (numItems >= MAX_INVENTORY_SIZE)
        {
            Debug.Log("Inventory is full!");
        } else
        {
            consumables.Add(consume);
            numItems = consumables.Count;
        }
    }

    public void RemoveItem(int consume)
    {
        if (numItems > 0)
        {
            consumables.Remove(consume);
            numItems = consumables.Count;
        } else
        {
            Debug.Log("Inventory is empty!");
        }
    }
}
