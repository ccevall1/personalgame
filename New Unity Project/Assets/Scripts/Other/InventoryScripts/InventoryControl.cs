﻿using UnityEngine;
using System.Collections;

public class InventoryControl : MonoBehaviour {

    GameController root;
    public bool isDirty;  //If true, need to reload info from PlayerPrefs
    public float movementSpeed;
    public Vector3 closedInventoryPosition;
    public Vector3 openInventoryPosition;
    //used for moving in transition states
    private Vector3 targetInventoryPosition;

    GameObject OpenCloseButton;

    public int 
        numAbilitiesUnlocked,
        questItemsObtained, 
        sideQuestOneObtained,
        sideQuestTwoObtained,
        sideQuestThreeObtained,
        sideQuestFourObtained,
        playerAttack, 
        playerAttackSpeed,
        playerMaxHealth,
        playerCurrentHealth,
        playerMaxMP,
        playerCurrentMP,
        dungeonKeys;

    private StatsTextControl statsText;
    private UIConsumablesControl UIConsumablesList;
    private UIKeyControl keyText;
    private KeyItemsControl keyItems;
    private UIAbilitiesControl abilities;

	// Use this for initialization
	void Start () {
        root = GameObject.Find("GameController").GetComponent<GameController>();
        OpenCloseButton = GameObject.Find("OpenCloseButton").gameObject;
        isDirty = true;
        statsText = transform.FindChild("Stats").GetComponent<StatsTextControl>();
        UIConsumablesList = transform.FindChild("UIConsumables").GetComponent<UIConsumablesControl>();
        keyText = transform.FindChild("Keys").GetComponent<UIKeyControl>();
        keyItems = transform.FindChild("KeyItems").GetComponent<KeyItemsControl>();
        abilities = transform.FindChild("Abilities").GetComponent<UIAbilitiesControl>();
        targetInventoryPosition = closedInventoryPosition;
    }

    // Update is called once per frame
    void Update () {
        //probably not the best place to put this logic, but I didn't want to clutter up GameController
	    if (root.inBattle)
        {
            OpenCloseButton.SetActive(false);
        }
        else
        {
            OpenCloseButton.SetActive(true);
        }

        if (Vector3.Distance(transform.position, targetInventoryPosition) > 0.1f)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, targetInventoryPosition, movementSpeed * Time.deltaTime);
        }
        else
        {
            transform.localPosition = targetInventoryPosition;
        }
	}

    //Grab all the current inventory data (if not grabbed yet) and display
    public void OpenInventory()
    {
        if (isDirty)
        {
            //Key Items/Abilities
            numAbilitiesUnlocked = root.SaveManager.playerAbilitiesUnlocked;
            questItemsObtained = root.SaveManager.questItemsObtained;
            sideQuestOneObtained = root.SaveManager.sideQuestOneObtained;
            sideQuestTwoObtained = root.SaveManager.sideQuestTwoObtained;
            sideQuestThreeObtained = root.SaveManager.sideQuestThreeObtained;
            sideQuestFourObtained = root.SaveManager.sideQuestFourObtained;

            //Player Stats
            playerAttack = root.SaveManager.playerAttack;
            playerAttackSpeed = root.SaveManager.playerAttackSpeed;
            playerMaxHealth = root.SaveManager.playerMaxHealth;
            playerCurrentHealth = root.SaveManager.playerCurrentHealth;
            playerMaxMP = root.SaveManager.playerMaxMP;
            playerCurrentMP = root.SaveManager.playerCurrentMP;
            dungeonKeys = root.SaveManager.dungeonKeys;
            isDirty = false;
        }

        OnRefresh();

        targetInventoryPosition = openInventoryPosition;

    }
    public void CloseInventory()
    {
        targetInventoryPosition = closedInventoryPosition;
    }

    public void OnRefresh()
    {
        //Refresh contents
        statsText.RefreshStatsText();
        UIConsumablesList.onRefresh(root.consumablesList.consumables);
        keyText.onRefresh();
        keyItems.onRefresh(questItemsObtained, sideQuestOneObtained,
                                               sideQuestTwoObtained,
                                               sideQuestThreeObtained,
                                               sideQuestFourObtained);
        abilities.onRefresh(numAbilitiesUnlocked - 1);  // -1 because 1st ability is not displayed (generic attack)
    }

}
