﻿using UnityEngine;
using System.Collections;

public class KeyItemsControl : MonoBehaviour {

    public GameObject[] keyItems;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void onRefresh(int numItems, int q1, int q2, int q3, int q4)
    {
        for (int i = 0; i < numItems; i++)
        {
            keyItems[2*i].SetActive(true);
        }
        if (q1 == 1) keyItems[1].SetActive(true);
        if (q2 == 1) keyItems[3].SetActive(true);
        if (q3 == 1) keyItems[5].SetActive(true);
        if (q4 == 1) keyItems[7].SetActive(true);
    }
}
