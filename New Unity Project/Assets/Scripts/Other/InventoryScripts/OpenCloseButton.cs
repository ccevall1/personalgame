﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class OpenCloseButton : MonoBehaviour {

    private GameController root;
    private bool isOpen;

	// Use this for initialization
	void Start () {
        GetComponent<TapGesture>().Tapped += tapHandler;
        root = transform.root.GetComponent<GameController>();
	}
	
	// Update is called once per frame
	void Update () {

	}

    private void tapHandler(object o, System.EventArgs e)
    {
        if (root.GamePaused)
        {
            root.OpenCloseInventory(-1);
            transform.localEulerAngles = new Vector3(0, 0, 180);
        }
        else
        {
            root.OpenCloseInventory(1);
            transform.localEulerAngles = new Vector3(0, 0, 0);
        }
    }
}
