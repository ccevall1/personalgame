﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StatsTextControl : MonoBehaviour {

    TextMesh baseText, healthText, MPText, attackText, speedText;
    InventoryControl parent;

    public int sortingOrder;

	// Use this for initialization
	void Start () {
        parent = transform.parent.GetComponent<InventoryControl>();

        //All the child texts
        baseText = transform.FindChild("StatsText").GetComponent<TextMesh>();
        healthText = transform.FindChild("StatsHealthText").GetComponent<TextMesh>();
        MPText = transform.FindChild("StatsMPText").GetComponent<TextMesh>();
        attackText = transform.FindChild("StatsAttackText").GetComponent<TextMesh>();
        speedText = transform.FindChild("StatsSpeedText").GetComponent<TextMesh>();

        //Set to appropriate layer because Unity won't let you do it from the editor
        baseText.gameObject.GetComponent<MeshRenderer>().sortingLayerName = "UI";
        baseText.gameObject.GetComponent<MeshRenderer>().sortingOrder = sortingOrder;
        healthText.gameObject.GetComponent<MeshRenderer>().sortingLayerName = "UI";
        healthText.gameObject.GetComponent<MeshRenderer>().sortingOrder = sortingOrder;
        MPText.gameObject.GetComponent<MeshRenderer>().sortingLayerName = "UI";
        MPText.gameObject.GetComponent<MeshRenderer>().sortingOrder = sortingOrder;
        attackText.gameObject.GetComponent<MeshRenderer>().sortingLayerName = "UI";
        attackText.gameObject.GetComponent<MeshRenderer>().sortingOrder = sortingOrder;
        speedText.gameObject.GetComponent<MeshRenderer>().sortingLayerName = "UI";
        speedText.gameObject.GetComponent<MeshRenderer>().sortingOrder = sortingOrder;

    }

    // Update is called once per frame
    void Update () {
	
	}

    public void RefreshStatsText()
    {
        healthText.text = parent.playerCurrentHealth + "/" + parent.playerMaxHealth;
        MPText.text = parent.playerCurrentMP + "/" + parent.playerMaxMP;
        attackText.text = parent.playerAttack + "";
        speedText.text = parent.playerAttackSpeed + "";
    }
}
