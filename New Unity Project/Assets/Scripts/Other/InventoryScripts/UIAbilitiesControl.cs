﻿using UnityEngine;
using System.Collections;

public class UIAbilitiesControl : MonoBehaviour {

    public GameObject[] abilities;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void onRefresh(int numAbilities)
    {
        for (int i = 0; i < numAbilities; i++)
        {
            abilities[i].SetActive(true);
        }
    }
}
