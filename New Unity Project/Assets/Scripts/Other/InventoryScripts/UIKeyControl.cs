﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIKeyControl : MonoBehaviour {

    InventoryControl parent;
    TextMesh keyText;

	// Use this for initialization
	void Start () {
        parent = transform.parent.GetComponent<InventoryControl>();
        keyText = transform.FindChild("KeyText").GetComponent<TextMesh>();
        keyText.GetComponent<MeshRenderer>().sortingLayerName = "UI";
        keyText.GetComponent<MeshRenderer>().sortingOrder = 0;
    }

    // Update is called once per frame
    void Update () {
	
	}

    public void onRefresh()
    {
        keyText.text = "x" + parent.dungeonKeys;
    }
}
