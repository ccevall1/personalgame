﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TouchScript.Gestures;

public class ConsumablesControl : MonoBehaviour {

    public int ID;
    public int HealthBoost, MPBoost;
    protected SpriteRenderer rend;
    public Sprite spr;
    public Color selectedColor;

    [HideInInspector] public bool isSelected;

    protected GameController root;
    UIConsumablesControl UIParent;

	// Use this for initialization
	void Start () {
        GetComponent<TapGesture>().Tapped += tapHandler;
        rend = GetComponent<SpriteRenderer>();
        spr = rend.sprite;
        root = transform.root.GetComponent<GameController>();
        UIParent = transform.parent.parent.GetComponent<UIConsumablesControl>();
        gameObject.SetActive(false);
	}

    // Update is called once per frame
    protected virtual void Update() {
        rend.sprite = spr;
        if (isSelected)
        {
            rend.color = selectedColor;
        } else
        {
            rend.color = Color.white;
        }
	}

    private void tapHandler(object o, System.EventArgs e)
    {
        Debug.Log("Tap!");
        //Broadcast to other consumables that this one was clicked (so others not selected anymore)
        foreach (ConsumablesControl con in UIParent.consumablesSlots)
        {
            if (con != this)
            {
                con.isSelected = false;
            }
        }

        if (isSelected)
        {
            root.OnConsumableUsed(ID, HealthBoost, MPBoost);
            isSelected = false;
        } else
        {
            isSelected = true;
        }

    }

}
