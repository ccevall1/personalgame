﻿using UnityEngine;
using System.Collections;

public class KeyboardCheats : MonoBehaviour {

    GameController root;

    private KeyCode[] NumberKeys = new KeyCode[]
    {
        KeyCode.Alpha0,
        KeyCode.Alpha1,
        KeyCode.Alpha2,
        KeyCode.Alpha3,
        KeyCode.Alpha4,
        KeyCode.Alpha5,
        KeyCode.Alpha6,
        KeyCode.Alpha7,
        KeyCode.Alpha8,
        KeyCode.Alpha9,
        KeyCode.F10,
        KeyCode.F1,
        KeyCode.F2,
        KeyCode.F3
    };

	// Use this for initialization
	void Start () {
        root = GetComponent<GameController>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftShift) && Input.GetKeyUp("r"))
        {
            Debug.Log("reloading scene");
            // Reload scene
            ExitControl tempDoor = new ExitControl();
            tempDoor.targetMap = root.currentScene;
            tempDoor.targetPosition = new Vector3(1, 1, 0);
            root.ChangeScene(tempDoor);
        }
        else if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyUp("k"))
        {
            // Add 1 key to inventory
            root.SaveManager.dungeonKeys++;
        }
        else if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyUp("e"))
        {
            // Disable currently active enemies
            GameObject.Find("EnemyContainer").SetActive(false);
        }
        else if (Input.GetKey(KeyCode.LeftAlt))
        {
            for (int i = 0; i < NumberKeys.Length; i++)
            {
                if (Input.GetKeyUp(NumberKeys[i]))
                {
                    ExitControl door = new ExitControl();
                    door.targetMap = i;
                    door.targetPosition = new Vector3(1, 1, 0);
                    root.ChangeScene(door);
                }
            }
        }

    }
}
