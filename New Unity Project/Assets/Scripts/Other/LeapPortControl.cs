﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class LeapPortControl : MonoBehaviour {

    private GameController root;
    public Vector3 leapDirection;
    public Vector3 leapTargetPosition;
    LineRenderer directionLine;

    private bool hasDirectionBeenSet;

	// Use this for initialization
	void Start () {
        GetComponent<FlickGesture>().Flicked += flickHandler;
        root = GameObject.Find("GameController").GetComponent<GameController>();
        directionLine = GetComponent<LineRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (!hasDirectionBeenSet)
        {
            directionLine.SetPosition(0, Vector3.zero);
            directionLine.SetPosition(1, 15*leapDirection);
            //directionLine.GetComponent<Renderer>().sortingLayerName = "Items";
            //directionLine.GetComponent<Renderer>().sortingLayerID = GetComponent<SpriteRenderer>().sortingLayerID;
            //directionLine.GetComponent<Renderer>().sortingOrder = GetComponent<SpriteRenderer>().sortingOrder;
            hasDirectionBeenSet = true;
        }
	}

    private void flickHandler(object o, System.EventArgs e)
    {
        FlickGesture flick = o as FlickGesture;
        Debug.Log("Flick: " + flick.ScreenFlickVector.normalized);
        Vector3 flickDir = flick.ScreenFlickVector.normalized;
        if (Vector3.Distance(flickDir, leapDirection) < 0.5f)
        {
            root.LeapPortFlickDetected(leapTargetPosition, transform.position);
        }
    }

}
