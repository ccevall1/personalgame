﻿using UnityEngine;
using System.Collections.Generic;

public class ObjectStateManager : MonoBehaviour {

    private Dictionary<string, int> ObjectStates;

	// Use this for initialization
	void Start () {
        ObjectStates = new Dictionary<string, int>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetState(string key, int value)
    {
        int val;
        if (ObjectStates.TryGetValue(key, out val))
        {
            ObjectStates.Remove(key);
        }
        ObjectStates.Add(key, value);
    }

    public int GetState(string key)
    {
        int value = -1;
        ObjectStates.TryGetValue(key, out value);
        return value;
    }
}
