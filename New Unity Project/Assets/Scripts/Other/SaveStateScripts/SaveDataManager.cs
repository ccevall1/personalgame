﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SaveDataManager : MonoBehaviour {

    public int 
        playerPositionX,
        playerPositionY,
        sceneID,
        playerAbilitiesUnlocked,
        playerLevel,
        playerEXP,
        playerMaxHealth,
        playerCurrentHealth,
        playerMaxMP,
        playerCurrentMP,
        playerAttack,
        playerDefense,
        playerAttackSpeed,
        humanAbilitiesUnlocked,
        humanEXP,
        dungeonKeys,
        questItemsObtained,
        sideQuestOneObtained,
        sideQuestTwoObtained,
        sideQuestThreeObtained,
        sideQuestFourObtained;


    public List<int> inventoryConsumables;

	// Use this for initialization
	void Start() {
        //int newGame = PlayerPrefs.GetInt("NewGame", -1);
        int newGame = -1;
        if (newGame == -1)
        {
            //Is a new game, delete all previous data
            StartNewGame();
            PlayerPrefs.SetInt("NewGame", 1);
        }

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void StartNewGame()
    {
        Debug.Log("Creating new game...");
        PlayerPrefs.DeleteAll();
        //Overworld Player Data
        PlayerPrefs.SetInt("PlayerPositionX", 1);
        PlayerPrefs.SetInt("PlayerPositionY", 1);
        PlayerPrefs.SetInt("SceneID", 0);
        PlayerPrefs.SetInt("PlayerAbilitiesUnlocked", 1);

        //Battle Player Data
        PlayerPrefs.SetInt("PlayerLevel", 1);
        PlayerPrefs.SetInt("PlayerEXP", 0);
        PlayerPrefs.SetInt("PlayerMaxHealth", 5);
        PlayerPrefs.SetInt("PlayerMaxMP", 5);
        PlayerPrefs.SetInt("PlayerCurrentHealth", 5);
        PlayerPrefs.SetInt("PlayerCurrentMP", 5);
        PlayerPrefs.SetInt("PlayerAttack", 1);
        PlayerPrefs.SetInt("PlayerDefense", 0);
        PlayerPrefs.SetInt("PlayerAttackSpeed", 1);
        //Battle Human Data
        PlayerPrefs.SetInt("HumanCooldownTime", 2);
        PlayerPrefs.SetInt("HumanAbilitiesUnlocked", 1);

        //Dungeon/Quest Data
        PlayerPrefs.SetInt("DungeonKeys", 0);
        PlayerPrefs.SetInt("QuestItemsObtained", 0);
        PlayerPrefs.SetInt("SideQuestOneObtained", 0);
        PlayerPrefs.SetInt("SideQuestTwoObtained", 0);
        PlayerPrefs.SetInt("SideQuestThreeObtained", 0);
        PlayerPrefs.SetInt("SideQuestourObtained", 0);


    }

    public void LoadGame()
    {
        Debug.Log("Loading Game...");
        //Overworld Player Data
        playerPositionX = PlayerPrefs.GetInt("PlayerPositionX", 1);
        playerPositionY = PlayerPrefs.GetInt("PlayerPositionY", 1);
        sceneID = PlayerPrefs.GetInt("SceneID", 0);
        playerAbilitiesUnlocked = PlayerPrefs.GetInt("PlayerAbilitiesUnlocked", 1);

        //Battle Player Data
        playerLevel = PlayerPrefs.GetInt("PlayerLevel", 1);
        playerEXP = PlayerPrefs.GetInt("PlayerEXP", 0);
        playerMaxHealth = PlayerPrefs.GetInt("PlayerMaxHealth", 5);
        playerMaxMP = PlayerPrefs.GetInt("PlayerMaxMP", 5);
        playerCurrentHealth = PlayerPrefs.GetInt("PlayerCurrentHealth", 5);
        playerCurrentMP = PlayerPrefs.GetInt("PlayerCurrentMP", 5);
        playerAttack = PlayerPrefs.GetInt("PlayerAttack", 1);
        playerDefense = PlayerPrefs.GetInt("PlayerDefense", 0);
        playerAttackSpeed = PlayerPrefs.GetInt("PlayerAttackSpeed", 1);
        //Battle Human Data
        humanEXP = PlayerPrefs.GetInt("HumanEXP", 0);
        humanAbilitiesUnlocked = PlayerPrefs.GetInt("HumanAbilitiesUnlocked", 1);

        //Dungeon Data
        dungeonKeys = PlayerPrefs.GetInt("DungeonKeys", 0);
        questItemsObtained = PlayerPrefs.GetInt("QuestItemsObtained", 0);
        sideQuestOneObtained = PlayerPrefs.GetInt("SideQuestOneObtained", 0);
        sideQuestTwoObtained = PlayerPrefs.GetInt("SideQuestTwoObtained", 0);
        sideQuestThreeObtained = PlayerPrefs.GetInt("SideQuestThreeObtained", 0);
        sideQuestFourObtained = PlayerPrefs.GetInt("SideQuestFourObtained", 0);

    }

    public void SaveGame()
    {
        Debug.Log("Saving...");
        //Overworld Player Data
        PlayerPrefs.SetInt("PlayerPositionX", playerPositionX);
        PlayerPrefs.SetInt("PlayerPositionY", playerPositionY);
        PlayerPrefs.SetInt("SceneID", sceneID);
        PlayerPrefs.SetInt("PlayerAbilitiesUnlocked", playerAbilitiesUnlocked);

        //Battle Player Data
        PlayerPrefs.SetInt("PlayerLevel", playerLevel);
        PlayerPrefs.SetInt("PlayerEXP", playerEXP);
        PlayerPrefs.SetInt("PlayerMaxHealth", playerMaxHealth);
        PlayerPrefs.SetInt("PlayerMaxMP", playerMaxMP);
        PlayerPrefs.SetInt("PlayerCurrentHealth", playerCurrentHealth);
        PlayerPrefs.SetInt("PlayerCurrentMP", playerCurrentMP);
        PlayerPrefs.SetInt("PlayerAttack", playerAttack);
        PlayerPrefs.SetInt("PlayerDefense", playerDefense);
        PlayerPrefs.SetInt("PlayerAttackSpeed", playerAttackSpeed);
        //Battle Human Data
        PlayerPrefs.SetInt("HumanEXP", humanEXP);
        PlayerPrefs.SetInt("HumanAbilitiesUnlocked", humanAbilitiesUnlocked);

        //Dungeon Data
        PlayerPrefs.SetInt("DungeonKeys", dungeonKeys);
        PlayerPrefs.SetInt("QuestItemsObtained", questItemsObtained);
        PlayerPrefs.SetInt("SideQuestOneObtained", sideQuestOneObtained);
        PlayerPrefs.SetInt("SideQuestTwoObtained", sideQuestTwoObtained);
        PlayerPrefs.SetInt("SideQuestThreeObtained", sideQuestThreeObtained);
        PlayerPrefs.SetInt("SideQuestourObtained", sideQuestFourObtained);
    }
}
