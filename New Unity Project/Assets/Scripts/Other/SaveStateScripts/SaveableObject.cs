﻿using UnityEngine;
using System.Collections;

public class SaveableObject : MonoBehaviour {

    ObjectStateManager StateManager;

	// Use this for initialization
	void Start () {
        StateManager = transform.root.GetComponent<GameController>().GetComponent<ObjectStateManager>();
        if (StateManager == null)
        {
            Debug.LogWarning("Warning: State Manager is null!");
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetSaveState(string key, int state)
    {
        StateManager.SetState(key, state);
    }

    public int GetSaveState(string key)
    {
        return StateManager.GetState(key);
    }
}
