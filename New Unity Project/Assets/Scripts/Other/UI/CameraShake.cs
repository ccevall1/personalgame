﻿// Attach to Camera

using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{

    private float shakeTimer;
    private float shakeAmount;
    private Vector3 defaultPosition;

    // Use this for initialization
    void Start()
    {
        defaultPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (shakeTimer > 0)
        {
            shakeTimer -= Time.deltaTime;
            Vector3 shakeVector = new Vector3(defaultPosition.x, defaultPosition.y + Random.Range(-shakeAmount, shakeAmount), defaultPosition.z);
            transform.position = shakeVector;
        }
        else
        {
            //transform.position = defaultPosition;
            shakeTimer = 0;
        }
    }

    public void shakeCamera(float shakeTime, float shake)
    {
        //save position
        defaultPosition = transform.position;
        shakeTimer = shakeTime;
        shakeAmount = shake;

    }
}
