﻿using UnityEngine;
using System.Collections;

public class LevelUpUpgradeSelectorControl : MonoBehaviour {

    public bool isOpen;
    BattleGameControl root;

    public float movementSpeed;

    public Vector3 openPosition;
    public Vector3 closedPosition;
    private Vector3 targetPosition;

    public GameObject LevelUpEffect;

	// Use this for initialization
	void Start () {
        root = transform.root.FindChild("BattleController").GetComponent<BattleGameControl>();
        targetPosition = closedPosition;
	}
	
	// Update is called once per frame
	void Update () {
	    if (Vector3.Distance(transform.localPosition, targetPosition) > 0.1f)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, targetPosition, movementSpeed * Time.deltaTime);
        }
        else
        {
            transform.localPosition = targetPosition;
        }
	}

    public void SelectUpgrade(int selection)
    {
        root.SelectUpgrade(selection);
    }

    public void OpenUpgradeSelection()
    {
        Instantiate(LevelUpEffect, transform.position + 20 * Vector3.left, Quaternion.identity);
        targetPosition = openPosition;
    }

    public void CloseUpgradeSelection()
    {
        targetPosition = closedPosition;
    }
}
