﻿using UnityEngine;
using System.Collections;

//Handles the placement of all objects in the UI
public class ScaleToCameraControl : MonoBehaviour {

    [HideInInspector] public float worldUnitsPerScreenPixel;
    [HideInInspector] public float cameraWidth;
    [HideInInspector] public float cameraHeight;
    private float sprWidth, sprHeight;

    private Camera cam;
    SpriteRenderer img;

    // Use this for initialization
    void Start () {
        img = GetComponent<SpriteRenderer>();

        sprWidth = 1; sprHeight = 1;
        if (img != null)
        {
            sprWidth = img.sprite.bounds.size.x / 2;
            sprHeight = img.sprite.bounds.size.y;
        }

        //Get camera info
        cam = GameObject.Find("MainCamera").GetComponent<Camera>();
        worldUnitsPerScreenPixel = cam.orthographicSize / cam.pixelHeight;
        cameraHeight = worldUnitsPerScreenPixel * cam.pixelHeight;
        cameraWidth = worldUnitsPerScreenPixel * cam.pixelWidth;

        //Scale gameobject to camera
        gameObject.transform.localScale = new Vector3(cameraWidth / sprWidth, cameraHeight / sprHeight * 2, 1);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
