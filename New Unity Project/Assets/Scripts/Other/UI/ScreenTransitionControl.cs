﻿using UnityEngine;
using System.Collections;

public class ScreenTransitionControl : MonoBehaviour {

    SpriteRenderer img;
    public float timeToDim;
    private float timeToDimInverse;
    private float timer;
    private Color invisible;
    public bool isTransitioning;
    public float currAlpha;

	// Use this for initialization
	void Start () {
        img = GetComponent<SpriteRenderer>();
        invisible = new Color(0, 0, 0, 0);
        timeToDimInverse = (1f / timeToDim);
        isTransitioning = false;
        timer = 0;
	}
	
	// Update is called once per frame
	void Update () {
        if (isTransitioning)
        {
            if (timer < timeToDim)
            {
                isTransitioning = true;
                currAlpha = -1 * ((timeToDimInverse * timer) * (timeToDimInverse * timer)) + 1;
                img.color = new Color(0, 0, 0, currAlpha);
                timer += Time.deltaTime;
            }
            else
            {
                isTransitioning = false;
            }
        }
        else
        {
            img.color = invisible;
        }
    }

    public void StartTransition()
    {
        timer = 0;
        img.color = Color.black;
        isTransitioning = true;
    }
}
