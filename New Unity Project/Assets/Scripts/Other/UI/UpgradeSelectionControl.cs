﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class UpgradeSelectionControl : MonoBehaviour {

    public int selectionID;
    LevelUpUpgradeSelectorControl parent;

	// Use this for initialization
	void Start () {
        parent = transform.parent.GetComponent<LevelUpUpgradeSelectorControl>();
        GetComponent<TapGesture>().Tapped += tapHandler;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void tapHandler(object o, System.EventArgs e)
    {
        parent.SelectUpgrade(selectionID);
    }
}
