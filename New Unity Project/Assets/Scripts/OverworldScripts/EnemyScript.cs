﻿using UnityEngine;
using System.Collections;

//This class constructs a reference to a battleEnemy object and on contact initializes a battle
public class EnemyScript : MonoBehaviour {

    private const int STATE_DEFEATED = 1;

    public int enemyID;
    public int maxHealth;
    public int attack;
    public int defense;
    public int speed;
    public float attackSpeed;
    public Vector3 enemyPosition;

    private SaveableObject SaveState;
    public bool IsOneShotEnemy;
    public string SaveStateID;

    // Use this for initialization
    void Awake () {
        SaveState = GetComponent<SaveableObject>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (IsOneShotEnemy)
        {
            Debug.Log("getting enemy state");
            if (SaveState != null)
            {
                int defeated = SaveState.GetSaveState(SaveStateID);
                Debug.Log("defeated is " + defeated);
                if (defeated == STATE_DEFEATED)
                {
                    gameObject.SetActive(false);
                }
            }
            else
            {
                SaveState = GetComponent<SaveableObject>();
            }
        }
    }

    public void OnDefeat()
    {
        if (IsOneShotEnemy && SaveState != null)
        {
            SaveState.SetSaveState(SaveStateID, STATE_DEFEATED);
        }
    }
}
