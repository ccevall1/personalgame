﻿using UnityEngine;
using System.Collections;

public class ExitDetails : ExitControl {

    public int tMap;
    public Vector3 tPosition;

    void Start()
    {
        targetMap = tMap;
        targetPosition = tPosition;
    }

}
