﻿//Game Controller class handles loading and setting up of scenes

using UnityEngine;
using System.Collections;
using System.Collections.Generic;       //Allows us to use Lists. 

public class GameController : MonoBehaviour
{
    public const int BATTLE_PLAYER_ID = 0;
    public static GameController instance = null;   //Static instance of GameManager which allows it to be accessed by any other script.
    public SaveDataManager SaveManager;             //Update info whenever relevant

    private FloorManager boardScript;               //Floor manager instantiates floor and wall tiles
    private MapReader mr;                           //Loads in text file and parses it into map for floor manager
    private BattleGameControl BattleController;     //Instance of battle scene manager

    private Camera mainCamera;
    private Vector3 cameraBattlePosition;
    private Vector3 cameraTargetPosition;
    private float worldUnitsPerScreenPixel;
    private float cameraWidth;
    private float cameraHeight;
    private CameraShake cameraShake;

    private ScreenTransitionControl screenTransition;

    [HideInInspector] public InventoryControl Inventory;
    public ConsumablesList consumablesList;

    private DialogueBoxControl DialogueBox;
    private DialogueTree currDialogue;

    public PlayerControl player;
    public DungeonPlayerControl dungeonPlayer;
    private DungeonControl DungeonController;

    public bool inBattle;
    public bool inDungeon;
    public bool GamePaused;
    public bool DialogueOpened;

    public SceneMetaData[] Scenes;
    public int currentScene;

    //Awake is always called before any Start functions
    void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

        SaveManager = GetComponent<SaveDataManager>();
        SaveManager.LoadGame();

        player = transform.FindChild("Player").GetComponent<PlayerControl>();
        BattleController = transform.FindChild("BattleController").GetComponent<BattleGameControl>();
        DungeonController = transform.FindChild("DungeonController").GetComponent<DungeonControl>();
        mainCamera = transform.FindChild("MainCamera").GetComponent<Camera>();
        cameraShake = mainCamera.gameObject.GetComponent<CameraShake>();
        cameraBattlePosition = new Vector3(BattleController.transform.position.x, BattleController.transform.position.y, -5);

        //Camera dimensions
        worldUnitsPerScreenPixel = mainCamera.orthographicSize / mainCamera.pixelHeight;
        cameraHeight = worldUnitsPerScreenPixel * mainCamera.pixelHeight;
        cameraWidth = worldUnitsPerScreenPixel * mainCamera.pixelWidth;

        screenTransition = mainCamera.transform.FindChild("ScreenTransition").GetComponent<ScreenTransitionControl>();

        //Init map before calling InitGame();
        boardScript = GetComponent<FloorManager>();
        mr = GetComponent<MapReader>();
        mr.currFile = Scenes[currentScene].mapFile;
        mr.InitMapReader();
        boardScript.map = mr;

        Inventory = GameObject.Find("Menu").GetComponent<InventoryControl>();
        consumablesList = GetComponent<ConsumablesList>();

        DialogueBox = GameObject.Find("DialogueBox").GetComponent<DialogueBoxControl>();
        DialogueBox.gameObject.SetActive(false);

        //Call the InitGame function to initialize the first level
        InitGame();
        Scenes[currentScene].beginScene();
    }

    //Initializes the game for each level.
    void InitGame()
    {
        //Instantiate Board and set boardHolder to its transform.
        boardScript.boardHolder = new GameObject("" + Scenes[currentScene].mapID).transform;
        Scenes[currentScene].hasBeenInitialized = true;
        boardScript.boardHolder.gameObject.transform.SetParent(Scenes[currentScene].transform);
        boardScript.SetupScene();
    }



    //Update is called every frame.
    void Update()
    {
        //Move Camera Appropriately
        if (inBattle)
        {
            if (Vector3.Distance(mainCamera.transform.position, cameraBattlePosition) > 0.1f)
            {
                mainCamera.transform.Translate((cameraBattlePosition - mainCamera.transform.position) * 0.5f);
            }
            else
            {
                mainCamera.transform.position = cameraBattlePosition;
            }
        }
        else if (inDungeon)
        {
            Vector3 playerPos = new Vector3(dungeonPlayer.transform.position.x, dungeonPlayer.transform.position.y, -5);
            if (Vector3.Distance(mainCamera.transform.position, playerPos) > 0.1f)
            {
                mainCamera.transform.Translate((playerPos - mainCamera.transform.position) * 0.5f);
            }
            else
            {
                mainCamera.transform.position = playerPos;
            }
            EnablePlayerMovement(true);

        }
        else
        {
            Vector3 playerPos = new Vector3(player.transform.position.x, player.transform.position.y, -5);
            if (Vector3.Distance(mainCamera.transform.position, playerPos) > 0.1f)
            {
                mainCamera.transform.Translate((playerPos - mainCamera.transform.position) * 0.5f);
                /*mainCamera.transform.position = new Vector3(
                    Mathf.Clamp(mainCamera.transform.position.x, cameraWidth-.5f, mr.numCols),
                    Mathf.Clamp(mainCamera.transform.position.y, cameraHeight-.5f, mr.numRows),
                    -5
                );*/
            } else
            {
                mainCamera.transform.position = playerPos;
                /*mainCamera.transform.position = new Vector3(
                    Mathf.Clamp(mainCamera.transform.position.x, cameraWidth, mr.numCols),
                    Mathf.Clamp(mainCamera.transform.position.y, cameraHeight, mr.numRows),
                    -5
                );*/
                EnablePlayerMovement(true);
            }
        }
        if (GamePaused)
        {
            Debug.Log("game is paused");
            EnablePlayerMovement(false);
        }

        if (screenTransition.isTransitioning)
        {
            EnablePlayerMovement(false);
        }
    }

    public void setMovement(int hor, int ver)
    {
        if (inDungeon)
        {
            dungeonPlayer.setMovement(hor, ver);
        } else
        {
            player.setMovement(hor, ver);
        }
    }

    public void EnablePlayerMovement(bool enable)
    {
        if (!inBattle)
        {
            if (inDungeon)
            {
                dungeonPlayer.canMove = enable;
            }
            else
            {
                player.canMove = enable;
            }
        }
        else
        {
            dungeonPlayer.canMove = false;
            player.canMove = false;
        }
    }

    public void initializeBattle(EnemyScript enemy)
    {
        inBattle = true;
        BattleController.initializeBattle(enemy);
        if (inDungeon)
        {
            BattleController.humanCharacter.gameObject.SetActive(false);
        } else
        {
            BattleController.humanCharacter.gameObject.SetActive(true);
        }
    }

    public void ChangeScene(ExitControl door)
    {
        screenTransition.StartTransition();

        //Position the player, deactivate old scene
        player.setPosition(door.targetPosition);
        player.dog.transform.position = player.transform.position;
        player.dog.targetPosition = player.transform.position;
        player.horizontal = 0; player.vertical = 0;
        Scenes[currentScene].endScene();    //Deactivates and deletes data

        //Grab the new target scene
        currentScene = door.targetMap;

        //If new scene already exists, reactivate it
        if (Scenes[currentScene].hasBeenInitialized)
        {
            Scenes[currentScene].beginScene();
        } else
        {
            mr.currFile = Scenes[currentScene].mapFile;
            mr.InitMapReader();
            InitGame();
            Scenes[currentScene].beginScene();  //Init enemies and activate scene
            Scenes[currentScene].hasBeenInitialized = true;
        }
    }

    public void LoadDungeon(ExitControl door)
    {
        screenTransition.StartTransition();

        int level = Scenes[currentScene].dungeonLevel - 1; // -1 for indexing
        if (level < 0)
        {
            // This means no level was specified
            return;
        }
        inDungeon = true;
        EnablePlayerMovement(true);

        Scenes[currentScene].endScene();
        DungeonController.Scenes[DungeonController.currentScene].endScene();

        if (DungeonController.Scenes[level].hasBeenInitialized)
        {
            DungeonController.Scenes[DungeonController.currentScene].beginScene();
        }
        else
        {
            mr.currFile = DungeonController.Scenes[DungeonController.currentScene].mapFile;
            mr.InitMapReader();
            DungeonController.InitGame();
            DungeonController.Scenes[DungeonController.currentScene].beginScene();  //Init data and activate scene
            DungeonController.Scenes[DungeonController.currentScene].hasBeenInitialized = true;
        }

        dungeonPlayer.gameObject.SetActive(true);
    }

    public void ExitDungeon(ExitControl door)
    {
        screenTransition.StartTransition();

        //Position the player, deactivate old scene
        player.setPosition(door.targetPosition);
        player.dog.transform.position = player.transform.position;
        player.dog.targetPosition = player.transform.position;
        player.horizontal = 0; player.vertical = 0;
        DungeonController.Scenes[DungeonController.currentScene].endScene();    //Deactivates object and deletes data
        dungeonPlayer.gameObject.SetActive(false);

        //Grab the new target scene
        currentScene = door.targetMap;

        //We use Scenes instead of DungeonController.Scenes because now we are assumed to be outside the dungeon
        if (Scenes[currentScene].hasBeenInitialized)
        {
            Scenes[currentScene].beginScene();
        }
        else
        {
            mr.currFile = Scenes[currentScene].mapFile;
            mr.InitMapReader();
            InitGame();
            Scenes[currentScene].beginScene();  //Init enemies and activate scene
            Scenes[currentScene].hasBeenInitialized = true;
        }
    }

    public void LeapPortFlickDetected(Vector3 targetPosition, Vector3 portPosition)
    {
        if (SaveManager.playerAbilitiesUnlocked < 2)
        {
            //Cannot leap until first ability has been unlocked
            return;
        }
        //Deal with leap actions
        if (inDungeon)
        {
            dungeonPlayer.LeapPortFlickDetected(targetPosition, portPosition);
        } else
        {
            player.LeapPortFlickDetected(targetPosition, portPosition);
        }
    }

    //1 -open, 0 -close
    public void OpenCloseInventory(int choice)
    {
        if (!inBattle && !DialogueOpened)
        {
            if (choice == 1)
            {
                Inventory.OpenInventory();
                GamePaused = true;
            } else
            {
                Inventory.CloseInventory();
                GamePaused = false;
            }
        }
    }

    public void OnConsumableUsed(int consum, int HP, int MP)
    {
        Inventory.CloseInventory();
        //Add health/MP value to player
        BattleController.OnItemUsed(BATTLE_PLAYER_ID, HP, MP);
        //Remove from list
        consumablesList.RemoveItem(consum);
        //Refresh contents
        Inventory.OnRefresh();
        //Since inventory closed, let player move again
        GamePaused = false;  //when we're in overworld, let's player move
        EnablePlayerMovement(true);
    }

    public bool TryUnlockDoor()
    {
        if (SaveManager.dungeonKeys > 0)
        {
            SaveManager.dungeonKeys--;
            Inventory.isDirty = true;
            return true;
        }
        else
        {
            return false;
        }
    }

    public void OpenDialogueTree(DialogueTree tree)
    {
        currDialogue = tree;
        DialogueOpened = true;
        DialogueBox.gameObject.SetActive(true);
        //Initialize text
        DialogueBox.SetText(currDialogue.firstLine);
        EnablePlayerMovement(false);
    }

    public void CloseDialogueTree()
    {
        DialogueOpened = false;
        DialogueBox.gameObject.SetActive(false);
        EnablePlayerMovement(true);
    }

    public void UpdateDialogue()
    {
        string nextText = currDialogue.AdvanceDialogue();
        Debug.Log("next text: " + nextText);
        // \n is indicator of end of dialogue
        if (!(nextText == "\n"))
        {
            DialogueBox.SetText(nextText);
        }
        else
        {
            CloseDialogueTree();
        }
    }

    public void AddScreenShake(float shakeTime, float shakeAmount)
    {
        cameraShake.shakeCamera(shakeTime, shakeAmount);
    }

}