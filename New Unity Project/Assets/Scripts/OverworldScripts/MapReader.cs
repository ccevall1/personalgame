﻿using UnityEngine;
using System.Collections;

public class MapReader : MonoBehaviour {

    [HideInInspector] public TextAsset currFile;
    private int currCol;
    private int currRow;
    [HideInInspector] public int numCols, numRows;
    private string[] lines;
    private int numEnemies;
    private Vector2 enemyPositions;

	// Use this for initialization
	void Start () {
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void InitMapReader()
    {
        //TODO: Detect rows and cols automatically
        lines = currFile.text.Split('\n');
        //Debug.Log("Num Rows: " + lines.Length);
        numRows = lines.Length;
        currRow = numRows - 1;
        currCol = 0;
        //Debug.Log("Num Cols: " + (lines[0].Length - 1));
        numCols = lines[0].Length - 1;
    }

    //Returns the tile type as an int
    public int nextCharacter()
    {
        //Debug.Log("currCol: " + currCol);
        //Debug.Log("currRow: " + currRow);
        int ret = lines[currRow][currCol] - '0';
        currCol++;
        if (currCol == numCols)
        {
            currCol = 0;
            currRow--;
        }
        return ret;
    }
}
