﻿using System;
using UnityEngine;

[Serializable]
public class MapStructure
{
    public string Layout;
    public int NumEnemies;
    //public Vector2[] Positions;
}
