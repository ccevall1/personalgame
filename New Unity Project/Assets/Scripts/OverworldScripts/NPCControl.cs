﻿using UnityEngine;
using System.Collections;
using TouchScript.Gestures;

public class NPCControl : MonoBehaviour {

    public DialogueTree[] dialogues;

    SpriteRenderer rend;
    private float movementTimer;
    private GameController root;

    public bool hasGift;
    public ChestContentsControl gift;

    private SaveableObject SaveState;
    public string SaveStateID;

	// Use this for initialization
	void Start () {
        root = transform.root.GetComponent<GameController>();
        rend = GetComponent<SpriteRenderer>();
        GetComponent<TapGesture>().Tapped += tapHandler;
        SaveState = GetComponent<SaveableObject>();
	}
	
	// Update is called once per frame
	void Update () {
	    if (movementTimer > 0)
        {
            movementTimer -= Time.deltaTime;
        }
        else
        {
            rend.flipX = !rend.flipX;
            movementTimer = .5f;
        }
	}

    private void tapHandler(object o, System.EventArgs e)
    {
        int dialogueChoice = (int) Mathf.Clamp(SaveState.GetSaveState(SaveStateID), 0, Mathf.Infinity);

        if (!root.DialogueOpened)
        {
            //See if player is close enough
            if (Vector3.Distance(transform.position, root.player.transform.position) < 1.1f)
            {
                root.OpenDialogueTree(dialogues[dialogueChoice]);
            }
        }
    }

    public void GivePlayerGift()
    {
        hasGift = false;
        ChestContentsControl o = Instantiate(gift, Vector3.zero, Quaternion.identity) as ChestContentsControl;
        o.transform.localPosition = Vector3.up;
        o.transform.SetParent(transform);
        o.Init();
        o.AddToInventory();
    }

    public void EndDialogue()
    {
        int currState = (int) Mathf.Clamp(SaveState.GetSaveState(SaveStateID), 0, Mathf.Infinity);
        SaveState.SetSaveState(SaveStateID, currState + 1);
    }
}
