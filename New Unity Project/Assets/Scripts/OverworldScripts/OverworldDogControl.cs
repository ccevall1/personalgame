﻿using UnityEngine;
using System.Collections;

public class OverworldDogControl : MonoBehaviour {

    [HideInInspector] public Vector3 targetPosition;
    public float speed;

	// Use this for initialization
	void Start () {
        targetPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (targetPosition != transform.position)
        {
            //transform.Translate(speed*(currPosition - transform.position), Space.World);
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
        }
        else if (Vector3.Distance(transform.position, targetPosition) < 0.1f)
        {
            transform.position = targetPosition;
        }
    }
}
