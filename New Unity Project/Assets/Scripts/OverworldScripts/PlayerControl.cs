﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

    public int horizontal, vertical;

    public float speed, leapSpeed;
    private float defaultSpeed;
    private Vector3 currPosition;
    private float inputDelay, delayTime;

    public bool canMove;

    private BoxCollider2D coll;
    private GameController root;

    private bool isTouchingLeapPort;
    private bool isLeaping;

    [HideInInspector] public OverworldDogControl dog;

    // Use this for initialization
    void Start () {
        currPosition = transform.position;
        defaultSpeed = speed;
        delayTime = 0.3f;
        inputDelay = 0f;

        coll = GetComponent<BoxCollider2D>();

        root = transform.root.GetComponent<GameController>();
        canMove = true;

        dog = GameObject.Find("OverworldDog").GetComponent<OverworldDogControl>();
	}
	
	// Update is called once per frame
	void Update () {

        /////////////////// Overworld Movement ////////////////////////////////
        if (horizontal != 0)
            vertical = 0;

        if (inputDelay > 0)
        {
            inputDelay -= Time.deltaTime;
        }
        else
        {
            if (horizontal != 0 || vertical != 0)
            {
                if (canMove && !root.GamePaused && !root.DialogueOpened)
                {
                    MovePlayer(horizontal, vertical);
                }
            }
        }

        if (currPosition != transform.position)
        {
            //transform.Translate(speed*(currPosition - transform.position), Space.World);
            transform.position = Vector3.MoveTowards(transform.position, currPosition, speed*Time.deltaTime);
        }
        else if (Vector3.Distance(transform.position, currPosition) < 0.1f)
        {
            transform.position = currPosition;
            speed = defaultSpeed;
        }

    }

    private void MovePlayer(int x, int y)
    {
        isTouchingLeapPort = false;
        coll.enabled = false;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, new Vector3(x, y, 0), 1.3f);
        coll.enabled = true;

        if (hit)
        {
            if (hit.transform.gameObject.tag == "Wall"
                || hit.transform.gameObject.tag == "NPC")
            {
                return;
            }
            else if (hit.transform.gameObject.tag == "Water")
            {
                //TODO: Swim ability
                return;
            }
            else if (hit.transform.gameObject.tag == "Enemy")
            {
                Debug.Log("Hit enemy");
                //Get enemy object
                EnemyScript enemy = hit.transform.gameObject.GetComponent<EnemyScript>();
                //Tell game controller we're battling and init enemies
                root.initializeBattle(enemy);
                //Disable movement
                root.EnablePlayerMovement(false);
                return;
            }
            else if (hit.transform.gameObject.tag == "Exit")
            {
                ExitControl exit = hit.transform.gameObject.GetComponent<ExitControl>();
                root.ChangeScene(exit);
            }
            else if (hit.transform.gameObject.tag == "WideExit")
            {
                ExitControl exit = hit.transform.gameObject.GetComponent<ExitControl>();
                exit.targetPosition = new Vector3(Mathf.Round(transform.position.x), exit.targetPosition.y, exit.targetPosition.z);
                root.ChangeScene(exit);
            }
            else if (hit.transform.gameObject.tag == "TallExit")
            {
                ExitControl exit = hit.transform.gameObject.GetComponent<ExitControl>();
                exit.targetPosition = new Vector3(exit.targetPosition.x, Mathf.Round(transform.position.y), exit.targetPosition.z);
                root.ChangeScene(exit);
            }
            else if (hit.transform.gameObject.tag == "DungeonDoor")
            {
                ExitControl exit = hit.transform.gameObject.GetComponent<ExitControl>();
                root.LoadDungeon(exit);
            }
            else if (hit.transform.gameObject.tag == "LeapPort")
            {
                //LeapPortControl leap = hit.transform.gameObject.GetComponent<LeapPortControl>();
                isTouchingLeapPort = true;
            }
            else
            {
                Debug.Log("Hit " + hit.transform.gameObject.tag);
            }
        }

        //Before you move, set the dog's position to be your current position
        dog.targetPosition = transform.position;
        currPosition = currPosition + new Vector3(x, y, 0);
        inputDelay = delayTime;
    }

    public void setMovement(int hor, int ver)
    {
        horizontal = hor;
        vertical = ver;
    }

    //For manual in-game setting of position
    public void setPosition(Vector3 newPosition)
    {
        Debug.Log("Setting Player's Position to " + newPosition);
        currPosition = newPosition;
        transform.position = currPosition;
    }

    public void LeapPortFlickDetected(Vector3 targetPosition, Vector3 portPosition)
    {
        if (transform.position != portPosition)
        {
            return;
        }

        Debug.Log("Leap!");
        currPosition = targetPosition;
        isTouchingLeapPort = false;
        isLeaping = true;
        speed = leapSpeed;
    }

}
