﻿using UnityEngine;
using System.Collections;

public class SceneMetaData : MonoBehaviour {

    //Scene Type
    public enum SceneType{ Dungeon, Overworld };
    public SceneType sceneType;

    //Map Data
    public TextAsset mapFile;
    public int mapID;
    public bool hasBeenInitialized;

    //Enemy Data
    public EnemyScript[] EnemyPrefabs;
    public Vector3[] enemyPositions;
    [HideInInspector] public Transform EnemyContainer;

    //Door Data
    public ExitControl[] DoorPrefabs;
    public Vector3[] DoorPositions;
    public int[] DoorTargetMapIDs;          //Refer to the position in the GameController's array of scenes
    public Vector3[] DoorTargetPositions;   //Where to place the player in the new scene
    [HideInInspector] public Transform DoorContainer;

    //Locked Door Data
    public LockedDoorControl[] LockedDoorPrefabs;
    public Vector3[] LockedDoorPositions;

    //Dungeon Door Data
    public int dungeonLevel;    //Reference to what dungeon the dungeon door leads to, assume <= 1 per scene

    //Abilities Data
    public LeapPortControl[] LeapPortPrefabs;
    public Vector3[] LeapPortPositions;
    public Vector3[] LeapPortTargetPositions;   //Where the leap port goes to
    public Vector3[] LeapPortDirections;        //What direction you have to flick to activate
    [HideInInspector] public Transform LeapPortContainer;

    //Treasure Chest Data
    public TreasureChestControl[] chestPrefabs;
    public Vector3[] ChestPositions;
    public TreasureChestContentsConstants.TreasureTypes[] chestContents;
    [HideInInspector] public Transform ChestContainer;

    //NPC Data
    public NPCControl[] NPCPrefabs;
    public Vector3[] NPCPositions;
    [HideInInspector] public Transform NPCContainer;

    //Called when a scene transition occurs from the GameController class
    public void endScene()
    {
        if (EnemyContainer)
            Destroy(EnemyContainer.gameObject);
        if (NPCContainer)
            Destroy(NPCContainer.gameObject);
        if (ChestContainer)
            Destroy(ChestContainer.gameObject);
        if (DoorContainer)
            Destroy(DoorContainer.gameObject);
        if (LeapPortContainer)
            Destroy(LeapPortContainer.gameObject);
        gameObject.SetActive(false);
    }

    public void beginScene()
    {
        //Initialize Doors
        int i = 0;
        if (!DoorContainer)
            DoorContainer = new GameObject("DoorContainer").transform;
        DoorContainer.transform.SetParent(gameObject.transform);
        DoorContainer.transform.localPosition = Vector3.zero;

        foreach (ExitControl door in DoorPrefabs)
        {
            ExitControl currDoor = (Instantiate(door.gameObject, Vector3.zero, Quaternion.identity) as GameObject).GetComponent<ExitControl>();
            currDoor.transform.SetParent(DoorContainer);
            currDoor.transform.localPosition = DoorPositions[i];
            currDoor.targetMap = DoorTargetMapIDs[i];
            currDoor.targetPosition = DoorTargetPositions[i];
            i++;
        }

        //Initialize Locked Doors
        i = 0;
        foreach (LockedDoorControl door in LockedDoorPrefabs)
        {
            LockedDoorControl currDoor = (Instantiate(door.gameObject, Vector3.zero, Quaternion.identity) as GameObject).GetComponent<LockedDoorControl>();
            currDoor.transform.SetParent(DoorContainer);
            currDoor.transform.localPosition = LockedDoorPositions[i];
            currDoor.SaveStateID = mapID + "_LockedDoor_" + i;
            i++;
        }

        //Initialize Enemies
        i = 0;
        if (!EnemyContainer)
            EnemyContainer = new GameObject("EnemyContainer").transform;
        EnemyContainer.transform.SetParent(gameObject.transform);
        EnemyContainer.transform.localPosition = Vector3.zero;

        foreach (EnemyScript enemy in EnemyPrefabs)
        {
            EnemyScript currEnemy = (Instantiate(enemy.gameObject, enemyPositions[i], Quaternion.identity) as GameObject).GetComponent<EnemyScript>();
            currEnemy.transform.SetParent(EnemyContainer);
            currEnemy.transform.localPosition = enemyPositions[i];
            currEnemy.SaveStateID = mapID + "_Enemy_" + i;
            i++;
        }

        //Initialize NPCs
        i = 0;
        if (!NPCContainer)
            NPCContainer = new GameObject("NPCContainer").transform;
        NPCContainer.transform.SetParent(gameObject.transform);
        NPCContainer.transform.localPosition = Vector3.zero;
        foreach (NPCControl npc in NPCPrefabs)
        {
            NPCControl currNPC = (Instantiate(npc.gameObject, NPCPositions[i], Quaternion.identity) as GameObject).GetComponent<NPCControl>();
            currNPC.transform.SetParent(NPCContainer);
            currNPC.transform.localPosition = NPCPositions[i];
            currNPC.SaveStateID = mapID + "_NPC_" + i;
            i++;
        }

        //Initialize Treasure Chests
        i = 0;
        if (!ChestContainer)
            ChestContainer = new GameObject("ChestContainer").transform;
        ChestContainer.transform.SetParent(gameObject.transform);
        ChestContainer.transform.localPosition = Vector3.zero;
        foreach (TreasureChestControl chest in chestPrefabs)
        {
            TreasureChestControl currChest = (Instantiate(chest.gameObject, ChestPositions[i], Quaternion.identity) as GameObject).GetComponent<TreasureChestControl>();
            currChest.transform.SetParent(ChestContainer);
            currChest.transform.localPosition = ChestPositions[i];
            currChest.contentsID = chestContents[i];
            currChest.SaveStateID = mapID + "_Chest_" + i;
            i++;
        }

        //Initialize Leap Ports
        i = 0;
        if (!LeapPortContainer)
            LeapPortContainer = new GameObject("LeapPortContainer").transform;
        LeapPortContainer.transform.SetParent(gameObject.transform);
        LeapPortContainer.transform.localPosition = Vector3.zero;
        foreach (LeapPortControl leap in LeapPortPrefabs)
        {
            LeapPortControl currLeapPort = (Instantiate(leap.gameObject, LeapPortPositions[i], Quaternion.identity) as GameObject).GetComponent<LeapPortControl>();
            currLeapPort.transform.SetParent(LeapPortContainer);
            currLeapPort.transform.localPosition = LeapPortPositions[i];
            currLeapPort.leapDirection = LeapPortDirections[i];
            currLeapPort.leapTargetPosition = LeapPortTargetPositions[i];
            i++;
        }

        gameObject.SetActive(true);
    }
}
