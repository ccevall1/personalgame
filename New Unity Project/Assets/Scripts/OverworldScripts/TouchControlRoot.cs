﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TouchScript.Gestures;

public class TouchControlRoot : MonoBehaviour {

    public float worldUnitsPerScreenPixel;
    public float cameraWidth;
    public float cameraHeight;

    private Camera cam;
    private BoxCollider2D boxCollider;
    private GameController root;

    public Image[] LeftMovementIndicators;
    public Image[] RightMovementIndicators;

    Color litMovementIndicatorColor;
    Color invisible;

	// Use this for initialization
	void Start () {
        GetComponent<ScreenTransformGesture>().Transformed += transformHandler;
        GetComponent<PressGesture>().Pressed += pressHandler;
        GetComponent<ReleaseGesture>().Released += releaseHandler;
        GetComponent<TapGesture>().Tapped += tapHandler;
        //GetComponent<FlickGesture>().Flicked += flickHandler;

        root = transform.root.GetComponent<GameController>();

        cam = transform.parent.GetComponent<Camera>();
        boxCollider = GetComponent<BoxCollider2D>();
        worldUnitsPerScreenPixel = cam.orthographicSize / cam.pixelHeight;
        cameraHeight = worldUnitsPerScreenPixel * cam.pixelHeight;
        cameraWidth = worldUnitsPerScreenPixel * cam.pixelWidth;
        boxCollider.size = new Vector2(cameraWidth * 2, cameraHeight * 2);

        litMovementIndicatorColor = new Color(1, 1, 1, 5.0f/255.0f);
        invisible = new Color(0, 0, 0, 0);
    }
	
	// Update is called once per frame
	void Update () {

	}

    private void transformHandler(object sender, System.EventArgs e)
    {
        int x, y;
        ScreenTransformGesture t = sender as ScreenTransformGesture;
        if (t.NormalizedScreenPosition.x < 0.3f)
        {
            x = -1;
        }
        else if (t.NormalizedScreenPosition.x > 0.7f)
        {
            x = 1;
        } else
        {
            x = 0;
        }

        if (t.NormalizedScreenPosition.y < 0.3f)
        {
            y = -1;
            x = 0;
        }
        else if (t.NormalizedScreenPosition.y > 0.7f)
        {
            y = 1;
            x = 0;
        }
        else
        {
            y = 0;
        }

        //Update movement indicators
        for (int i = 0; i < LeftMovementIndicators.Length; i++)
        {
            LeftMovementIndicators[i].color = invisible;
            RightMovementIndicators[i].color = invisible;
        }

        if (!root.inBattle)
        {
            if (t.NormalizedScreenPosition.x < 0.3f)
            {
                Debug.Log(LeftMovementIndicators[y + 1].gameObject.name + " getting pressed");
                LeftMovementIndicators[y + 1].color = litMovementIndicatorColor;
            }
            else if (t.NormalizedScreenPosition.x > 0.7f)
            {
                Debug.Log(RightMovementIndicators[y + 1].gameObject.name + " getting pressed");
                RightMovementIndicators[y + 1].color = litMovementIndicatorColor;
            }
        }

        root.setMovement(x, y);
        
    }

    private void pressHandler(object sender, System.EventArgs e)
    {
        int x, y;
        PressGesture t = sender as PressGesture;
        if (t.NormalizedScreenPosition.x < 0.3f)
        {
            x = -1;
        }
        else if (t.NormalizedScreenPosition.x > 0.7f)
        {
            x = 1;
        } else
        {
            x = 0;
        }

        if (t.NormalizedScreenPosition.y < 0.3f)
        {
            y = -1;
            x = 0;
        }
        else if (t.NormalizedScreenPosition.y > 0.7f)
        {
            y = 1;
            x = 0;
        }
        else
        {
            y = 0;
        }

        //Update movement indicators
        for (int i = 0; i < LeftMovementIndicators.Length; i++)
        {
            LeftMovementIndicators[i].color = invisible;
            RightMovementIndicators[i].color = invisible;
        }

        if (!root.inBattle)
        {
            if (t.NormalizedScreenPosition.x < 0.5f)
            {
                Debug.Log(LeftMovementIndicators[y + 1].gameObject.name + " getting pressed");
                LeftMovementIndicators[y + 1].color = litMovementIndicatorColor;
            }
            else if (t.NormalizedScreenPosition.x > 0.5f)
            {
                Debug.Log(RightMovementIndicators[y + 1].gameObject.name + " getting pressed");
                RightMovementIndicators[y + 1].color = litMovementIndicatorColor;
            }
        }

        root.setMovement(x, y);

    }

    private void releaseHandler(object sender, System.EventArgs e)
    {
        root.setMovement(0, 0);
        for (int i = 0; i < LeftMovementIndicators.Length; i++)
        {
            LeftMovementIndicators[i].color = invisible;
            RightMovementIndicators[i].color = invisible;
        }
    }

    private void flickHandler(object o, System.EventArgs e)
    {
        FlickGesture flick = o as FlickGesture;
        Vector3 dir = flick.ScreenFlickVector.normalized;
        if (dir.y > 0.3f)
        {
            Debug.Log("flick up");
        //    root.OpenCloseInventory(1);
        } else if (dir.y < -0.3f)
        {
            Debug.Log("flick down");
        //    root.OpenCloseInventory(0);
        }
    }

    private void tapHandler(object o, System.EventArgs e)
    {
        if (root.DialogueOpened)
        {
            root.UpdateDialogue();
        }
    }

}
