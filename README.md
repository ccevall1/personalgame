# README #

Personal project, Unity built mobile RPG including a demo up to the end of the first level.

Android apk included in the Builds folder.

All art is temporary and none of it is mine. Floor tiles, player sprite, and others taken from Unity's Roguelike tutorial, everything else found with Google images.